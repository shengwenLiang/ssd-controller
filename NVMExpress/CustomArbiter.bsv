/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
// Name of the Package		: Arbiter
// Coded by                     : M S Santosh Kumar
//
// Module Description		: This package contains normal and sticky round robin arbiters
//                                based on Parallel Prefix Computation
//
//Functionality at a glance	: 1. Parametrizable clients
//                                2. Traditional BSV arbiter library interface
//                                3. O(logN) delay with N clients
//			               
////////////////////////////////////////////////////////////////////////////////

package CustomArbiter;

import Vector::*;
import BUtils::*;
import Connectable::*;

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

interface ArbiterClient_IFC;
   method Action request();
   method Action lock();
   method Bool grant();
endinterface

interface ArbiterRequest_IFC;
   method Bool request();
   method Bool lock();
   method Action grant();
endinterface

interface Arbiter_IFC#(numeric type count);
   interface Vector#(count, ArbiterClient_IFC) clients;
   method    Bit#(TLog#(count))                grant_id;
endinterface

////////////////////////////////////////////////////////////////////////////////
/// A ppc based round robin arbiter with changing priorities. If the value of "fixed"
/// is True, the current grant is locked and not updated again until
/// "fixed" goes False; The functionality of fixed is not currently implemented
////////////////////////////////////////////////////////////////////////////////


// advances by one level in ppc OR logic
 function Vector#(nClients, Bool) ppc_advance_level 
    (Vector#(nClients,Bool) vec_inputs, Integer group_size);

    Vector#(nClients, Bool) vec_outputs = vec_inputs;
    for (Integer i = group_size; i < valueOf(nClients); i = i + 2*group_size)
       begin
	  for (Integer j = i; (j <= i + group_size) && (j < valueOf(nClients)); j = j+1)
	     begin
		vec_outputs[j] = vec_inputs[i-1] || vec_outputs[j];
	     end
       end
    return vec_outputs;

 endfunction


 module mkArbiter#(Bool fixed) (Arbiter_IFC#(count));

    let icount = valueOf(count);

    Reg#(Vector#(count, Bool)) mask_vector <- mkReg(replicate(False));

    Wire#(Vector#(count, Bool)) grant_vector   <- mkBypassWire;
    Wire#(Bit#(TLog#(count)))   grant_id_wire  <- mkBypassWire;
    Vector#(count, PulseWire) request_vector <- replicateM(mkPulseWire);

    function Bool getValue(PulseWire ifc);
       return ifc._read;
    endfunction

    
    // calculate grant vector
    rule calculate_grant(True);

       Vector#(count,Bool) unmasked_req_vector = map(getValue,request_vector);
       Vector#(count,Bool) masked_req_vector;
       Vector#(count,Bool) therm_enc_vector; // vector in thermometer encoding
       Vector#(count,Bool) lv_grant_vector;
       
       // bitwise OR
       
       masked_req_vector = unpack(pack(mask_vector) & pack(unmasked_req_vector));
       
       if (any(isTrue, masked_req_vector))
	  therm_enc_vector = masked_req_vector;
       else
	  therm_enc_vector = unpack(pack(unmasked_req_vector));

       // thermometer encoding using ppc
       for (Integer i = 1; i < icount; i = 2*i) begin
	  therm_enc_vector = ppc_advance_level(therm_enc_vector,i);
       end

       mask_vector <= unpack(pack(therm_enc_vector)<<1);
       lv_grant_vector = unpack(pack(therm_enc_vector) ^ (pack(therm_enc_vector)<<1));
       grant_vector <= lv_grant_vector;

       /*
       $display("request_vector %b",unmasked_req_vector);
       $display("masted request vector %b",masked_req_vector);
       $display("thermo encoded vector %b",therm_enc_vector);
       $display("mask_vector %b",mask_vector);
       $display("grant_vector %b",lv_grant_vector);
       */
    
    endrule : calculate_grant

    // It is prefarable not to use grant_id for checking grant since
    // this is a priority logic hopefully removed in logic optimization
    // during synthesis
    rule calculate_grant_id(True);
  
       Bit#(TLog#(count)) lv_grant_id = 0;
       for (Integer i = 0; i < icount; i = i + 1) begin
	  if (grant_vector[i] == True) begin
	     lv_grant_id = fromInteger(i);
	  end
       end
       grant_id_wire <= lv_grant_id;
       
    endrule : calculate_grant_id

   // Now create the vector of interfaces
   Vector#(count, ArbiterClient_IFC) client_vector = newVector;

   for (Integer x = 0; x < icount; x = x + 1)

      client_vector[x] = (interface ArbiterClient_IFC

			     method Action request();
				request_vector[x].send();
			     endmethod

			     method Action lock();
				dummyAction;
			     endmethod

			     method grant ();
				return grant_vector[x];
			     endmethod
			  endinterface);

   interface clients = client_vector;
   method    grant_id = grant_id_wire;
endmodule

////////////////////////////////////////////////////////////////////////////////
/// This one gives the current owner priority (i.e. they can hold as long as
/// they keep requesting it.
////////////////////////////////////////////////////////////////////////////////

 module mkStickyArbiter (Arbiter_IFC#(count));

    let icount = valueOf(count);

    Reg#(Vector#(count, Bool)) mask_vector <- mkReg(replicate(False));

    Wire#(Vector#(count, Bool)) grant_vector   <- mkBypassWire;
    Wire#(Bit#(TLog#(count)))   grant_id_wire  <- mkBypassWire;
    Vector#(count, PulseWire) request_vector <- replicateM(mkPulseWire);

    function Bool getValue(PulseWire ifc);
       return ifc._read;
    endfunction

    
    rule calculate_grant(True);

       Vector#(count,Bool) unmasked_req_vector = map(getValue,request_vector);
       Vector#(count,Bool) masked_req_vector;
       Vector#(count,Bool) therm_enc_vector; // vector in thermometer encoding
       Vector#(count,Bool) lv_grant_vector;
       
       // bitwise OR
       
       masked_req_vector = unpack(pack(mask_vector) & pack(unmasked_req_vector));
       
       if (any(isTrue, masked_req_vector))
	  therm_enc_vector = masked_req_vector;
       else
	  therm_enc_vector = unpack(pack(unmasked_req_vector));

       // thermometer encoding using ppc
       for (Integer i = 1; i < icount; i = 2*i) begin
	  therm_enc_vector = ppc_advance_level(therm_enc_vector,i);
       end

       mask_vector <= unpack(pack(therm_enc_vector));
       lv_grant_vector = unpack(pack(therm_enc_vector) ^ (pack(therm_enc_vector)<<1));
       grant_vector <= lv_grant_vector;

       /*
       $display("request_vector %b",unmasked_req_vector);
       $display("masted request vector %b",masked_req_vector);
       $display("thermo encoded vector %b",therm_enc_vector);
       $display("mask_vector %b",mask_vector);
       $display("grant_vector %b",lv_grant_vector);
       */
       
    endrule : calculate_grant

    // It is prefarable not to use grant_id for checking grant since
    // this is a priority logic hopefully removed in logic optimization
    // during synthesis
    rule calculate_grant_id(True);
  
       Bit#(TLog#(count)) lv_grant_id = 0;
       for (Integer i = 0; i < icount; i = i + 1) begin
	  if (grant_vector[i] == True) begin
	     lv_grant_id = fromInteger(i);
	  end
       end
       grant_id_wire <= lv_grant_id;
       
    endrule : calculate_grant_id

   // Now create the vector of interfaces
   Vector#(count, ArbiterClient_IFC) client_vector = newVector;

   for (Integer x = 0; x < icount; x = x + 1)

      client_vector[x] = (interface ArbiterClient_IFC

			     method Action request();
				request_vector[x].send();
			     endmethod

			     method Action lock();
				dummyAction;
			     endmethod

			     method grant ();
				return grant_vector[x];
			     endmethod
			  endinterface);

   interface clients = client_vector;
   method    grant_id = grant_id_wire;
endmodule

function Bool isTrue(Bool value);
   return value;
endfunction

instance Connectable#(ArbiterClient_IFC,  ArbiterRequest_IFC);
   module mkConnection#(ArbiterClient_IFC client, ArbiterRequest_IFC request) (Empty);

      rule send_grant (client.grant);
	 request.grant();
      endrule

      rule send_request (request.request);
	 client.request();
      endrule

      rule send_lock (request.lock);
	 client.lock();
      endrule
   endmodule
endinstance

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

typeclass Arbitable#(type a);
   module mkArbiterRequest#(a ifc) (ArbiterRequest_IFC);
endtypeclass

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

endpackage
