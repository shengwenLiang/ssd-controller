/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Testbench for DDR Connection
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu
last update done on 12th June 2014

This testbench test the DDR connection.

*/

package tb_ddr_connection;

import DReg::*;

import ddr_connection::*;
import global_definitions::*;

typedef enum {
	IDLE,
	INIT,
	REQUEST_NAND_TO_MM,
	NAND_TO_MM,
	REQUEST_MM_TO_NAND,
	MM_TO_NAND,
	WRITE,
	WAIT,
	REQUEST_READ,
	GET_READ,
	WRITE_RECEIVED
} Tb_state_type deriving (Bits, Eq);

interface Ifc_tb_for_ddr_connection;
	interface Ifc_read_data_ddr ifc_read_data_ddr;
	interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	interface Ifc_write_data_ddr ifc_write_data_ddr;
	interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method Action _reg_in(Bit#(32) _data);
	method Bit#(32) reg_out_();
	method Bit#(8) leds_();
endinterface

(* synthesize *)
(* always_ready *)
(* always_enabled *)
module mkTb_for_ddr_connection(Ifc_tb_for_ddr_connection);

Ifc_ddr_connection lv_ddr_connection <- mkDdr_connection;

Reg#(Tb_state_type) rg_tb_state <- mkReg(IDLE);
Reg#(UInt#(32)) rg_count <- mkReg(0);

Reg#(Bit#(32)) rg_last_data <- mkReg(0);
Reg#(UInt#(16)) rg_nr_pages <- mkReg('h2);
Reg#(bit) rg_counter_sel <- mkReg(1'b0);
Reg#(Bool) rg_pagewise <- mkReg(False);
Reg#(Bool) drg_restart <- mkDReg(False);

Reg#(Bit#(8)) rg_out_leds <- mkReg('hAA);
Reg#(UInt#(32)) rg_counter <- mkReg(0);

let lv_nand_flash_model = lv_ddr_connection.ifc_nand_flash_model;
let lv_main_memory_model = lv_ddr_connection.ifc_main_memory_model;
let lv_read_data_ddr = lv_ddr_connection.ifc_read_data_ddr;
let lv_read_cmd_ddr = lv_ddr_connection.ifc_read_cmd_ddr;
let lv_write_data_ddr = lv_ddr_connection.ifc_write_data_ddr;
let lv_write_cmd_ddr = lv_ddr_connection.ifc_write_cmd_ddr;
let lv_write_sts_ddr = lv_ddr_connection.ifc_write_sts_ddr;


/* temporary test signals */
Wire#(bit) dwr_is_last <- mkDWire(1'b0);

/* Counter which can be used by the software. */
Reg#(Bit#(64)) rg_nand2mm_ns_count <- mkReg(0);  // clk frequency of 100 MHz is assumed
Reg#(Bool) rg_nand2mm_count_started <- mkReg(False);
Reg#(Bit#(64)) rg_mm2nand_ns_count <- mkReg(0);  // clk frequency of 100 MHz is assumed
Reg#(Bool) rg_mm2nand_count_started <- mkReg(False);

rule rl_nand2mm_ns_count;
	/* Rule for sqid1 execution time counter used by software.

	100 MHz clock is assumed.
	*/
	if (rg_nand2mm_count_started) begin
		if (rg_tb_state == REQUEST_MM_TO_NAND)
			rg_nand2mm_count_started <= False;
		else
			rg_nand2mm_ns_count <= rg_nand2mm_ns_count + 10;
	end else if (rg_tb_state == IDLE && drg_restart == True) begin
		rg_nand2mm_ns_count <= 0;
		rg_nand2mm_count_started <= True;
	end
endrule: rl_nand2mm_ns_count

rule rl_mm2nand_ns_count;
	/* Rule for sqid1 execution time counter used by software.

	100 MHz clock is assumed.
	*/
	if (rg_mm2nand_count_started) begin
		if (rg_tb_state == IDLE && drg_restart == False)
			rg_mm2nand_count_started <= False;
		else
			rg_mm2nand_ns_count <= rg_mm2nand_ns_count + 10;
	end else if (rg_tb_state == REQUEST_MM_TO_NAND) begin
		rg_mm2nand_ns_count <= 0;
		rg_mm2nand_count_started <= True;
	end
endrule: rl_mm2nand_ns_count



rule rl_enable_nand;
	lv_nand_flash_model._enable(1'b0);
endrule: rl_enable_nand



rule rl_tb_idle_state (rg_tb_state == IDLE);
	if (drg_restart == True)
		rg_tb_state <= REQUEST_NAND_TO_MM;
endrule: rl_tb_idle_state


rule rl_tb_request_nand_to_mm_state (rg_tb_state == REQUEST_NAND_TO_MM);
	if (rg_pagewise)
		lv_nand_flash_model._request_data('h00020 + extend(unpack(pack(rg_count)[31:10])), 1);
	else
		lv_nand_flash_model._request_data('h00020, truncate(rg_nr_pages));
	rg_tb_state <= NAND_TO_MM;
endrule: rl_tb_request_nand_to_mm_state

rule rl_tb_nand_to_mm_state (rg_tb_state == NAND_TO_MM &&
		lv_main_memory_model.nvm_put_data_ready_ == 1'b1);
	let lv_data <- lv_nand_flash_model._get_data_();
	/* this is always pagewise */
	lv_main_memory_model._nvm_put_data(
		lv_data, 'h00030000 + 4 * extend(rg_count), 1024);// rg_nr_pages * 1024);
	if (rg_count == extend(rg_nr_pages) * 1024 - 1) begin
		rg_count <= 0;
		rg_tb_state <= REQUEST_MM_TO_NAND;
	end else if(rg_pagewise && pack(rg_count)[9:0] == 1023) begin
		rg_count <= rg_count + 1;
		rg_tb_state <= REQUEST_NAND_TO_MM;
	end else begin
		rg_count <= rg_count + 1;
	end
endrule: rl_tb_nand_to_mm_state



rule rl_tb_request_mm_to_nand_state (rg_tb_state == REQUEST_MM_TO_NAND);
	/* this is always pagewise */
	lv_main_memory_model._nvm_request_data('h00030000 + 4 * extend(rg_count), 1024);
	rg_tb_state <= MM_TO_NAND;
endrule: rl_tb_request_mm_to_nand_state

rule rl_tb_mm_to_nand_state (rg_tb_state == MM_TO_NAND);
	let lv_data <- lv_main_memory_model._nvm_get_data_();
	if (rg_pagewise)
		lv_nand_flash_model._write('h00040 + extend(unpack(pack(rg_count)[31:10])),
			lv_data, 1);
	else
		lv_nand_flash_model._write('h00040, lv_data, truncate(rg_nr_pages));
	if (rg_count == extend(rg_nr_pages) * 1024 - 1) begin
		rg_count <= 0;
		rg_tb_state <= IDLE;
	end else if (rg_pagewise && pack(rg_count)[9:0] == 1023) begin
		rg_count <= rg_count + 1;
		rg_tb_state <= REQUEST_MM_TO_NAND;
	end else begin
		rg_count <= rg_count + 1;
	end
endrule: rl_tb_mm_to_nand_state



interface Ifc_read_data_ddr ifc_read_data_ddr;
	method bit ready_();
		return lv_read_data_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_read_data_ddr._valid(_is_valid);
	endmethod: _valid

	method Action _data_in(Bit#(32) _data);
		lv_read_data_ddr._data_in(_data);
	endmethod: _data_in

	method Action _last(bit _is_last);
		lv_read_data_ddr._last(_is_last);
		dwr_is_last <= _is_last;
	endmethod: _last
endinterface: ifc_read_data_ddr

interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	method bit valid_();
		return lv_read_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_read_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_read_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_read_cmd_ddr

interface Ifc_write_data_ddr ifc_write_data_ddr;
	method bit valid_();
		return lv_write_data_ddr.valid_();
	endmethod: valid_

	method Bit#(32) data_out_();
		return lv_write_data_ddr.data_out_();
	endmethod: data_out_

	method bit last_();
		return lv_write_data_ddr.last_();
	endmethod: last_

	method Action _ready(bit _is_ready);
		lv_write_data_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_data_ddr

interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	method bit valid_();
		return lv_write_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_write_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_write_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_cmd_ddr

interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method bit ready_();
		return lv_write_sts_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_write_sts_ddr._valid(_is_valid);
		if (_is_valid == 1'b1)
			rg_out_leds <= rg_out_leds + 1;
	endmethod: _valid

	method Action _data_in(Bit#(8) _data);
		lv_write_sts_ddr._data_in(_data);
	endmethod: _data_in
endinterface: ifc_write_sts_ddr

method Action _reg_in(Bit#(32) _data);
	if (_data == rg_last_data + 1) begin
		drg_restart <= True;
	end else if (_data != rg_last_data) begin
		rg_nr_pages <= unpack(_data[15:0]);
		rg_counter_sel <= _data[30];
		rg_pagewise <= unpack(_data[31]);
	end
	rg_last_data <= _data;
endmethod: _reg_in

method Bit#(32) reg_out_();
	if (rg_counter_sel == 0)
		return rg_nand2mm_ns_count[31:0];
	else
		return rg_mm2nand_ns_count[31:0];
endmethod: reg_out_

method Bit#(8) leds_();
	//return lv_ddr_connection_part.leds_();
	return rg_out_leds;
endmethod: leds_

endmodule: mkTb_for_ddr_connection
endpackage: tb_ddr_connection

