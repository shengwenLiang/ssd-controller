package NvmWrapperHeader;

/*--- debug struct ---*/
import global_definitions::*;

interface Ifc_config_space;
   method Action _cfg_in_reqID( Bit#(8) _bus_number,
			       Bit#(5) _device_number,
			       Bit#(3) _function_number );
endinterface

interface Ifc_rx_packet;
   method Action _rx_in (  Bit#(128) _tdata,
			 Bool _tvalid,
			 Bit#(7) _bar_hit );
   method Action _rx_in_tuser_sof ( Bool _new_packet_assert,
				   Bit#(4) _new_byte_loc
				   );
   method Action _rx_in_tuser_eof ( Bool _end_packet_assert,
				   Bit#(4) _end_byte_loc
				   );
   method Action _rx_in_tuser_err (bit _fwd,
				   bit _ecrc_err);
   method Action _rx_msienable(bit _msienable);
   method Action _rx_cfg_intrpt_rdyN(bit _readyn);
   method Action _rx_cfg_intrpt_mmenable(Bit#(3) _mmenable);
   method bit rx_out_tready();
   method bit rx_out_np_ok();
   method bit rx_out_np_req();     
endinterface

interface Ifc_tx_packet;
   
   method Bit#(128) _tx_data_out ();
   method Bool _tx_out_eof_assert ();
   method Bit#(16) _tx_out_byte_enable ();
   method Bool _tx_out_tvalid ();
   method Action _tx_in_tready (Bool _pcie_ready);
   method Bool _tx_out_src_dsc ();
   method Action _tx_in_buf_av (Bit#(6) _tx_buf_av);
   method Action _tx_in_err_drop	(Bool _tx_err_drop);
   method Bool _tx_out_packet_stream ();
   method Action _tx_in_cfg_req (Bool _pcie_cfg_req);
   method Bool _tx_in_cfg_gnt ();
   method Bit#(8) _tx_cfg_interruptDI();
   method bit _tx_cfg_intrptN();
   method Bit#(5) _tx_out_debug();
   method Completion_status_type _tx_out_compl_status();
   method Bit#(16) _tx_out_debug_info();
   method Bool _tx_out_err_fwd ();
   method Bool _tx_out_ecrc_gen ();

endinterface

interface Ifc_debug;

   method Bit#(32) _valid_packet_counter ();
   method Bit#(32) _write_packet_counter ();
   method Bit#(32) _read_packet_counter ();
   method Bit#(32) _write_packet_address ();
   method Bit#(32) _invalid_packet_counter ();
   method Bit#(32) _first_header();
   method Bit#(32) _compl_received();
   method Bit#(32) _new_byte_loc();
   method Bit#(32) _transfered_data_msb();
   method Bit#(32) _transfered_data_lsb();
   method Bit#(32) _written_data_msb();
   method Bit#(32) _written_data_lsb();
   method Bit#(32) _read_packet_address();

endinterface
endpackage
