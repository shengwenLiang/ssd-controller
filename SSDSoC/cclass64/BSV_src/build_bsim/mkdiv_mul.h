/*
 * Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
 * 
 * On Tue Jul 19 13:21:16 IST 2016
 * 
 */

/* Generation options: */
#ifndef __mkdiv_mul_h__
#define __mkdiv_mul_h__

#include "bluesim_types.h"
#include "bs_module.h"
#include "bluesim_primitives.h"
#include "bs_vcd.h"


/* Class declaration for the mkdiv_mul module */
class MOD_mkdiv_mul : public Module {
 
 /* Clock handles */
 private:
  tClock __clk_handle_0;
 
 /* Clock gate handles */
 public:
  tUInt8 *clk_gate[0];
 
 /* Instantiation parameters */
 public:
 
 /* Module state */
 public:
  MOD_Reg<tUInt8> INST_div_or_rem;
  MOD_Reg<tUInt8> INST_div_type;
  MOD_Reg<tUWide> INST_partial;
  MOD_Reg<tUWide> INST_partial_prod;
  MOD_Reg<tUWide> INST_rg_inp1;
  MOD_Reg<tUInt8> INST_rg_inp1_sign;
  MOD_Reg<tUWide> INST_rg_inp2;
  MOD_Reg<tUInt8> INST_rg_inp2_sign;
  MOD_Reg<tUInt8> INST_rg_mul_or_div;
  MOD_Reg<tUInt32> INST_rg_state_counter;
  MOD_Wire<tUInt32> INST_rg_state_counter_1;
  MOD_Reg<tUInt8> INST_rg_take_complement;
  MOD_Reg<tUInt8> INST_rg_word;
  MOD_Reg<tUWide> INST_wr_final_result;
  MOD_Wire<tUWide> INST_wr_final_result_1;
 
 /* Constructor */
 public:
  MOD_mkdiv_mul(tSimStateHdl simHdl, char const *name, Module *parent);
 
 /* Symbol init methods */
 private:
  void init_symbols_0();
 
 /* Reset signal definitions */
 private:
  tUInt8 PORT_RST_N;
 
 /* Port definitions */
 public:
  tUWide PORT_result_;
 
 /* Publicly accessible definitions */
 public:
  tUInt32 DEF_b__h841;
 
 /* Local definitions */
 private:
  tUInt8 DEF_div_or_rem__h3394;
  tUInt8 DEF_y__h1976;
  tUInt8 DEF_div_type__h3388;
  tUInt64 DEF_v__h4638;
  tUInt64 DEF_v__h3168;
  tUInt64 DEF_v__h2472;
  tUInt64 DEF_v__h2355;
  tUWide DEF_partial_prod__h3026;
  tUWide DEF_p__h4135;
  tUWide DEF_d__h1063;
  tUWide DEF_x__h3083;
  tUWide DEF_wr_final_result_1_wget____d2;
  tUWide DEF_wr_final_result___d201;
  tUInt8 DEF_rg_word__h3254;
  tUWide DEF_x_BITS_127_TO_0___h3411;
  tUWide DEF_x__h3409;
  tUWide DEF_v_BITS_127_TO_0___h3412;
  tUWide DEF_IF_partial_prod_6_BITS_1_TO_0_7_EQ_0b1_8_THEN__ETC___d122;
  tUWide DEF_partial_prod_BITS_128_TO_2___h3124;
  tUWide DEF_x__h4624;
  tUWide DEF_x__h4104;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_0_67_PLUS_S_ETC___d176;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_0_67_PLUS_S_ETC___d175;
  tUWide DEF_z1__h2350;
  tUWide DEF__theResult___snd__h1067;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_rg_inp1_3_B_ETC___d33;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_rg_inp1_3_B_ETC___d32;
  tUWide DEF_partial_1_BITS_65_TO_0___d22;
  tUWide DEF_x_BITS_64_TO_0___h3084;
  tUWide DEF_IF_IF_div_type_3_AND_NOT_div_or_rem_2_1_29_THE_ETC___d139;
  tUWide DEF_spliced_bits__h2959;
  tUWide DEF_y__h4171;
  tUWide DEF_y__h4224;
  tUWide DEF_y__h1097;
  tUWide DEF_y__h1150;
  tUWide DEF_temp___1__h2620;
  tUWide DEF_temp___1__h2912;
  tUWide DEF_temp__h3103;
  tUWide DEF_temp___1__h2765;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_0_67_PLUS_S_ETC___d177;
  tUWide DEF_partial_1_BITS_65_TO_0_2_CONCAT_0___d167;
  tUWide DEF_IF_rg_state_counter_4_EQ_32_0_THEN_0_ELSE_IF_I_ETC___d39;
  tUInt8 DEF_rg_state_counter_4_EQ_32___d20;
  tUWide DEF_IF_partial_1_BITS_65_TO_0_2_CONCAT_rg_inp1_3_B_ETC___d35;
  tUWide DEF_partial_1_BITS_65_TO_0_2_CONCAT_rg_inp1_3_BIT__ETC___d25;
  tUWide DEF_IF_start_mul_or_div_THEN_IF_start__div_type_TH_ETC___d188;
  tUWide DEF_x__h4590;
  tUWide DEF__0_CONCAT_IF_start__div_type_AND_NOT_start__div_ETC___d187;
  tUInt32 DEF_IF_rg_state_counter_4_EQ_32_0_THEN_0_ELSE_rg_s_ETC___d41;
  tUInt32 DEF_rg_state_counter_4_PLUS_1___d40;
  tUInt8 DEF_NOT_div_or_rem_2___d81;
  tUInt8 DEF_NOT_div_type_3___d44;
  tUWide DEF_SEXT__3_CONCAT_INV_IF_start__div_type_THEN_IF__ETC___d171;
  tUWide DEF_x__h4327;
  tUWide DEF_SEXT_INV_rg_inp2_6_7_PLUS_1_8___d29;
  tUWide DEF_x__h1251;
  tUWide DEF_INV_IF_partial_prod_6_BITS_1_TO_0_7_EQ_0b1_8_T_ETC___d135;
  tUWide DEF_INV_rg_inp2_6___d27;
  tUWide DEF__3_CONCAT_INV_IF_start__div_type_THEN_IF_start__ETC___d169;
  tUWide DEF_spliced_bits__h2932;
  tUWide DEF__0_CONCAT_0_CONCAT_partial_prod_6_BITS_127_TO_6_ETC___d113;
  tUWide DEF__0_CONCAT_partial_prod_6_BITS_127_TO_64_9___d100;
  tUWide DEF_spliced_bits__h2785;
  tUWide DEF__0_CONCAT_partial_prod_6_BITS_128_TO_65_06___d107;
  tUWide DEF__0_CONCAT_IF_start__div_type_AND_NOT_start__div_ETC___d200;
  tUWide DEF__0_CONCAT_IF_start_mul_or_div_THEN_IF_start__di_ETC___d162;
  tUWide DEF__theResult___fst__h1066;
  tUWide DEF__1_CONCAT_IF_div_or_rem_2_THEN_IF_NOT_div_type__ETC___d80;
  tUWide DEF__1_CONCAT_IF_NOT_div_or_rem_2_1_AND_NOT_div_typ_ETC___d142;
  tUWide DEF_wr_final_result_1_whas_AND_0_OR_wr_final_resul_ETC___d9;
 
 /* Rules */
 public:
  void RL_wr_final_result__dreg_update();
  void RL_rg_state_counter__dreg_update();
  void RL_division();
  void RL_multiply();
 
 /* Methods */
 public:
  void METH__start(tUInt64 ARG__start_inp1,
		   tUInt64 ARG__start_inp2,
		   tUInt8 ARG__start__div_type,
		   tUInt8 ARG__start__div_or_rem,
		   tUInt8 ARG__start_mul_or_div,
		   tUInt8 ARG__start_word);
  tUInt8 METH_RDY__start();
  tUWide METH_result_();
  tUInt8 METH_RDY_result_();
 
 /* Reset routines */
 public:
  void reset_RST_N(tUInt8 ARG_rst_in);
 
 /* Static handles to reset routines */
 public:
 
 /* Pointers to reset fns in parent module for asserting output resets */
 private:
 
 /* Functions for the parent module to register its reset fns */
 public:
 
 /* Functions to set the elaborated clock id */
 public:
  void set_clk_0(char const *s);
 
 /* State dumping routine */
 public:
  void dump_state(unsigned int indent);
 
 /* VCD dumping routines */
 public:
  unsigned int dump_VCD_defs(unsigned int levels);
  void dump_VCD(tVCDDumpType dt, unsigned int levels, MOD_mkdiv_mul &backing);
  void vcd_defs(tVCDDumpType dt, MOD_mkdiv_mul &backing);
  void vcd_prims(tVCDDumpType dt, MOD_mkdiv_mul &backing);
};

#endif /* ifndef __mkdiv_mul_h__ */
