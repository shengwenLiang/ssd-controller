/*
 * Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
 * 
 * On Tue Jul 19 13:21:16 IST 2016
 * 
 */
#include "bluesim_primitives.h"
#include "module_fn_branch.h"


/* Constructor */
MOD_module_fn_branch::MOD_module_fn_branch(tSimStateHdl simHdl, char const *name, Module *parent)
  : Module(simHdl, name, parent),
    DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81(194u),
    DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52(65u)
{
  PORT_fn_branch.setSize(263u);
  PORT_fn_branch.clear();
  symbol_count = 1u;
  symbols = new tSym[symbol_count];
  init_symbols_0();
}


/* Symbol init fns */

void MOD_module_fn_branch::init_symbols_0()
{
  init_symbol(&symbols[0u], "fn_branch", SYM_PORT, &PORT_fn_branch, 263u);
}


/* Rule actions */


/* Methods */

tUWide MOD_module_fn_branch::METH_fn_branch(tUInt8 ARG_fn_branch__dest_addr,
					    tUInt8 ARG_fn_branch__opcode,
					    tUInt8 ARG_fn_branch__funct3,
					    tUInt64 ARG_fn_branch__current_pc,
					    tUInt32 ARG_fn_branch__immediate_value,
					    tUInt64 ARG_fn_branch__operand1,
					    tUInt64 ARG_fn_branch__operand2,
					    tUInt8 ARG_fn_branch__prediction)
{
  tUInt8 DEF_NOT_fn_branch__opcode_EQ_0b11001___d19;
  tUInt8 DEF_NOT_fn_branch__opcode_EQ_0b11011___d18;
  tUInt8 DEF_NOT_fn_branch__opcode_EQ_0b11011_8_AND_NOT_fn__ETC___d69;
  tUInt8 DEF_NOT_fn_branch__operand1_BIT_63_6___d33;
  tUInt8 DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29;
  tUInt8 DEF_NOT_fn_branch__opcode_EQ_0b11000_4_0_OR_IF_fn__ETC___d49;
  tUInt8 DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67;
  tUInt8 DEF_NOT_fn_branch__prediction___d53;
  tUInt8 DEF_fn_branch__funct3_EQ_0b111___d41;
  tUInt8 DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28;
  tUInt8 DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30;
  tUInt8 DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31;
  tUInt8 DEF_fn_branch__operand1_EQ_fn_branch__operand2___d22;
  tUInt8 DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65;
  tUInt8 DEF_NOT_fn_branch__operand1_EQ_fn_branch__operand2_2___d23;
  tUInt8 DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48;
  tUInt8 DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d6;
  tUInt8 DEF_fn_branch__opcode_EQ_0b11011___d1;
  tUInt64 DEF_lv_effective__h96;
  tUInt8 DEF_fn_branch__opcode_EQ_0b11001___d2;
  tUInt64 DEF_lv_effective__h158;
  tUInt8 DEF_fn_branch__opcode_EQ_0b11000___d14;
  tUInt64 DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5;
  tUInt64 DEF_fn_branch__current_pc_PLUS_4___d4;
  tUInt64 DEF_x__h112;
  tUInt8 DEF_fn_branch__operand1_BIT_63___d26;
  tUInt8 DEF_y__h379;
  tUInt64 DEF_lv_target_offset__h95;
  DEF_lv_target_offset__h95 = primSignExt64(64u, 12u, (tUInt32)(ARG_fn_branch__immediate_value));
  DEF_y__h379 = (tUInt8)(ARG_fn_branch__operand2 >> 63u);
  DEF_fn_branch__operand1_BIT_63___d26 = (tUInt8)(ARG_fn_branch__operand1 >> 63u);
  DEF_fn_branch__current_pc_PLUS_4___d4 = ARG_fn_branch__current_pc + 4llu;
  switch (ARG_fn_branch__opcode) {
  case (tUInt8)25u:
  case (tUInt8)27u:
    DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5 = DEF_fn_branch__current_pc_PLUS_4___d4;
    break;
  default:
    DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5 = 0llu;
  }
  DEF_fn_branch__opcode_EQ_0b11000___d14 = ARG_fn_branch__opcode == (tUInt8)24u;
  DEF_lv_effective__h158 = ((((tUInt64)(9223372036854775807llu & DEF_lv_target_offset__h95)) << 1u) | (tUInt64)((tUInt8)0u)) + ARG_fn_branch__current_pc;
  DEF_fn_branch__opcode_EQ_0b11001___d2 = ARG_fn_branch__opcode == (tUInt8)25u;
  DEF_lv_effective__h96 = (((tUInt64)((DEF_lv_target_offset__h95 + ARG_fn_branch__operand1) >> 1u)) << 1u) | (tUInt64)((tUInt8)0u);
  switch (ARG_fn_branch__opcode) {
  case (tUInt8)24u:
  case (tUInt8)27u:
    DEF_x__h112 = DEF_lv_effective__h158;
    break;
  case (tUInt8)25u:
    DEF_x__h112 = DEF_lv_effective__h96;
    break;
  default:
    DEF_x__h112 = 0llu;
  }
  DEF_fn_branch__opcode_EQ_0b11011___d1 = ARG_fn_branch__opcode == (tUInt8)27u;
  switch (ARG_fn_branch__opcode) {
  case (tUInt8)25u:
  case (tUInt8)27u:
    DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d6 = ARG_fn_branch__dest_addr;
    break;
  default:
    DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d6 = (tUInt8)0u;
  }
  DEF_fn_branch__operand1_EQ_fn_branch__operand2___d22 = ARG_fn_branch__operand1 == ARG_fn_branch__operand2;
  DEF_NOT_fn_branch__operand1_EQ_fn_branch__operand2_2___d23 = !DEF_fn_branch__operand1_EQ_fn_branch__operand2___d22;
  DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30 = ARG_fn_branch__operand1 < ARG_fn_branch__operand2;
  DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31 = !DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30;
  DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28 = DEF_fn_branch__operand1_BIT_63___d26 == DEF_y__h379;
  DEF_fn_branch__funct3_EQ_0b111___d41 = ARG_fn_branch__funct3 == (tUInt8)7u;
  DEF_NOT_fn_branch__prediction___d53 = !ARG_fn_branch__prediction;
  DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29 = !DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28;
  DEF_NOT_fn_branch__operand1_BIT_63_6___d33 = !DEF_fn_branch__operand1_BIT_63___d26;
  switch (ARG_fn_branch__funct3) {
  case (tUInt8)0u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = DEF_NOT_fn_branch__operand1_EQ_fn_branch__operand2_2___d23;
    break;
  case (tUInt8)1u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = DEF_fn_branch__operand1_EQ_fn_branch__operand2___d22;
    break;
  case (tUInt8)4u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = (DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29 || DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31) && (DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28 || DEF_NOT_fn_branch__operand1_BIT_63_6___d33);
    break;
  case (tUInt8)5u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = (DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29 || DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30) && (DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28 || DEF_fn_branch__operand1_BIT_63___d26);
    break;
  case (tUInt8)6u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31;
    break;
  default:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48 = !DEF_fn_branch__funct3_EQ_0b111___d41 || DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30;
  }
  switch (ARG_fn_branch__funct3) {
  case (tUInt8)0u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = DEF_fn_branch__operand1_EQ_fn_branch__operand2___d22;
    break;
  case (tUInt8)1u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = DEF_NOT_fn_branch__operand1_EQ_fn_branch__operand2_2___d23;
    break;
  case (tUInt8)4u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = (DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28 && DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30) || (DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29 && DEF_fn_branch__operand1_BIT_63___d26);
    break;
  case (tUInt8)5u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = (DEF_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ope_ETC___d28 && DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31) || (DEF_NOT_fn_branch__operand1_BIT_63_6_EQ_fn_branch__ETC___d29 && DEF_NOT_fn_branch__operand1_BIT_63_6___d33);
    break;
  case (tUInt8)6u:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = DEF_fn_branch__operand1_ULT_fn_branch__operand2___d30;
    break;
  default:
    DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65 = DEF_fn_branch__funct3_EQ_0b111___d41 && DEF_NOT_fn_branch__operand1_ULT_fn_branch__operand2_0___d31;
  }
  DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67 = DEF_fn_branch__opcode_EQ_0b11011___d1 || (DEF_fn_branch__opcode_EQ_0b11000___d14 && DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_fn_branch___ETC___d65);
  DEF_NOT_fn_branch__opcode_EQ_0b11000_4_0_OR_IF_fn__ETC___d49 = !DEF_fn_branch__opcode_EQ_0b11000___d14 || DEF_IF_fn_branch__funct3_EQ_0b0_1_THEN_NOT_fn_bran_ETC___d48;
  DEF_NOT_fn_branch__opcode_EQ_0b11011___d18 = !DEF_fn_branch__opcode_EQ_0b11011___d1;
  DEF_NOT_fn_branch__opcode_EQ_0b11011_8_AND_NOT_fn__ETC___d69 = DEF_NOT_fn_branch__opcode_EQ_0b11011___d18 && DEF_NOT_fn_branch__opcode_EQ_0b11000_4_0_OR_IF_fn__ETC___d49;
  DEF_NOT_fn_branch__opcode_EQ_0b11001___d19 = !DEF_fn_branch__opcode_EQ_0b11001___d2;
  DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52.set_bits_in_word((tUInt8)(DEF_x__h112 >> 63u),
										2u,
										0u,
										1u).set_whole_word((tUInt32)(DEF_x__h112 >> 31u),
												   1u).set_whole_word((((tUInt32)(2147483647u & DEF_x__h112)) << 1u) | (tUInt32)(DEF_NOT_fn_branch__opcode_EQ_0b11011___d18 && (DEF_NOT_fn_branch__opcode_EQ_0b11001___d19 && DEF_NOT_fn_branch__opcode_EQ_0b11000_4_0_OR_IF_fn__ETC___d49)),
														      0u);
  DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.set_bits_in_word((tUInt8)(ARG_fn_branch__current_pc >> 62u),
										6u,
										0u,
										2u).set_whole_word((tUInt32)(ARG_fn_branch__current_pc >> 30u),
												   5u).set_whole_word((((tUInt32)(1073741823u & ARG_fn_branch__current_pc)) << 2u) | (tUInt32)(primExtract8(2u,
																									    65u,
																									    DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52,
																									    32u,
																									    64u,
																									    32u,
																									    63u)),
														      4u).set_whole_word(primExtract32(32u,
																		       65u,
																		       DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52,
																		       32u,
																		       62u,
																		       32u,
																		       31u),
																	 3u).build_concat(((((tUInt64)(DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52.get_bits_in_word32(0u,
																														       0u,
																														       31u))) << 33u) | (((tUInt64)(DEF_NOT_fn_branch__opcode_EQ_0b11001___d19 && ((DEF_NOT_fn_branch__prediction___d53 && DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67) || (ARG_fn_branch__prediction && DEF_NOT_fn_branch__opcode_EQ_0b11011_8_AND_NOT_fn__ETC___d69)))) << 32u)) | (tUInt64)((tUInt32)((DEF_fn_branch__opcode_EQ_0b11001___d2 || ((ARG_fn_branch__prediction || DEF_NOT_fn_branch__opcode_EQ_0b11011_8_AND_NOT_fn__ETC___d69) && (DEF_NOT_fn_branch__prediction___d53 || DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67)) ? (DEF_fn_branch__opcode_EQ_0b11001___d2 || (ARG_fn_branch__prediction && DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67) ? DEF_x__h112 : DEF_fn_branch__current_pc_PLUS_4___d4) : DEF_x__h112) >> 32u)),
																			  32u,
																			  64u).set_whole_word((tUInt32)(DEF_fn_branch__opcode_EQ_0b11001___d2 || ((ARG_fn_branch__prediction || DEF_NOT_fn_branch__opcode_EQ_0b11011_8_AND_NOT_fn__ETC___d69) && (DEF_NOT_fn_branch__prediction___d53 || DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67)) ? (DEF_fn_branch__opcode_EQ_0b11001___d2 || (ARG_fn_branch__prediction && DEF_fn_branch__opcode_EQ_0b11011_OR_fn_branch__opc_ETC___d67) ? DEF_x__h112 : DEF_fn_branch__current_pc_PLUS_4___d4) : DEF_x__h112),
																					      0u);
  PORT_fn_branch.set_bits_in_word((tUInt8)(DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5 >> 57u),
				  8u,
				  0u,
				  7u).set_whole_word((tUInt32)(DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5 >> 25u),
						     7u).set_whole_word(((((tUInt32)(33554431u & DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d5)) << 7u) | (((tUInt32)(DEF_IF_fn_branch__opcode_EQ_0b11011_OR_fn_branch___ETC___d6)) << 2u)) | (tUInt32)(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_bits_in_word8(6u,
																																										     0u,
																																										     2u)),
									6u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(5u),
											   5u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(4u),
													      4u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(3u),
																 3u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(2u),
																		    2u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(1u),
																				       1u).set_whole_word(DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81.get_whole_word(0u),
																							  0u);
  return PORT_fn_branch;
}

tUInt8 MOD_module_fn_branch::METH_RDY_fn_branch()
{
  tUInt8 PORT_RDY_fn_branch;
  tUInt8 DEF_CAN_FIRE_fn_branch;
  DEF_CAN_FIRE_fn_branch = (tUInt8)1u;
  PORT_RDY_fn_branch = DEF_CAN_FIRE_fn_branch;
  return PORT_RDY_fn_branch;
}


/* Reset routines */


/* Static handles to reset routines */


/* Functions for the parent module to register its reset fns */


/* Functions to set the elaborated clock id */


/* State dumping routine */
void MOD_module_fn_branch::dump_state(unsigned int indent)
{
}


/* VCD dumping routines */

unsigned int MOD_module_fn_branch::dump_VCD_defs(unsigned int levels)
{
  vcd_write_scope_start(sim_hdl, inst_name);
  vcd_num = vcd_reserve_ids(sim_hdl, 3u);
  unsigned int num = vcd_num;
  for (unsigned int clk = 0u; clk < bk_num_clocks(sim_hdl); ++clk)
    vcd_add_clock_def(sim_hdl, this, bk_clock_name(sim_hdl, clk), bk_clock_vcd_num(sim_hdl, clk));
  vcd_write_def(sim_hdl, num++, "IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52", 65u);
  vcd_write_def(sim_hdl, num++, "fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81", 194u);
  vcd_write_def(sim_hdl, num++, "fn_branch", 263u);
  vcd_write_scope_end(sim_hdl);
  return num;
}

void MOD_module_fn_branch::dump_VCD(tVCDDumpType dt,
				    unsigned int levels,
				    MOD_module_fn_branch &backing)
{
  vcd_defs(dt, backing);
}

void MOD_module_fn_branch::vcd_defs(tVCDDumpType dt, MOD_module_fn_branch &backing)
{
  unsigned int num = vcd_num;
  if (dt == VCD_DUMP_XS)
  {
    vcd_write_x(sim_hdl, num++, 65u);
    vcd_write_x(sim_hdl, num++, 194u);
    vcd_write_x(sim_hdl, num++, 263u);
  }
  else
    if (dt == VCD_DUMP_CHANGES)
    {
      if ((backing.DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52) != DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52)
      {
	vcd_write_val(sim_hdl, num, DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52, 65u);
	backing.DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52 = DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52;
      }
      ++num;
      if ((backing.DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81) != DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81)
      {
	vcd_write_val(sim_hdl, num, DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81, 194u);
	backing.DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81 = DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81;
      }
      ++num;
      if ((backing.PORT_fn_branch) != PORT_fn_branch)
      {
	vcd_write_val(sim_hdl, num, PORT_fn_branch, 263u);
	backing.PORT_fn_branch = PORT_fn_branch;
      }
      ++num;
    }
    else
    {
      vcd_write_val(sim_hdl, num++, DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52, 65u);
      backing.DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52 = DEF_IF_fn_branch__opcode_EQ_0b11011_THEN_SEXT_fn_b_ETC___d52;
      vcd_write_val(sim_hdl, num++, DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81, 194u);
      backing.DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81 = DEF_fn_branch__current_pc_CONCAT_IF_fn_branch__opc_ETC___d81;
      vcd_write_val(sim_hdl, num++, PORT_fn_branch, 263u);
      backing.PORT_fn_branch = PORT_fn_branch;
    }
}
