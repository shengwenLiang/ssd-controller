/*
 * Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
 * 
 * On Tue Jul 19 13:21:16 IST 2016
 * 
 */

/* Generation options: */
#ifndef __mkriscv_h__
#define __mkriscv_h__

#include "bluesim_types.h"
#include "bs_module.h"
#include "bluesim_primitives.h"
#include "bs_vcd.h"
#include "mkexecution_unit.h"
#include "mkmemory_unit.h"
#include "mkregisterfile.h"
#include "module_fn_decoder.h"


/* Class declaration for the mkriscv module */
class MOD_mkriscv : public Module {
 
 /* Clock handles */
 private:
  tClock __clk_handle_0;
 
 /* Clock gate handles */
 public:
  tUInt8 *clk_gate[0];
 
 /* Instantiation parameters */
 public:
 
 /* Module state */
 public:
  MOD_mkexecution_unit INST_alu_unit;
  MOD_Fifo<tUWide> INST_ff_id_ie;
  MOD_Fifo<tUWide> INST_ff_ie_imem;
  MOD_Fifo<tUWide> INST_ff_if_id;
  MOD_Fifo<tUWide> INST_ff_imem_iwb;
  MOD_mkmemory_unit INST_mem_unit;
  MOD_mkregisterfile INST_register_file;
  MOD_Reg<tUWide> INST_rg_data_from_instruction_memory;
  MOD_Wire<tUWide> INST_rg_data_from_instruction_memory_1;
  MOD_Wire<tUWide> INST_rg_data_to_instruction_memory;
  MOD_Reg<tUInt64> INST_rg_pc;
  MOD_Wire<tUInt64> INST_wr_effective_address;
  MOD_Wire<tUInt8> INST_wr_flush_everything;
  MOD_Wire<tUWide> INST_wr_forward_from_MEM;
  MOD_module_fn_decoder INST_instance_fn_decoder_0;
 
 /* Constructor */
 public:
  MOD_mkriscv(tSimStateHdl simHdl, char const *name, Module *parent);
 
 /* Symbol init methods */
 private:
  void init_symbols_0();
 
 /* Reset signal definitions */
 private:
  tUInt8 PORT_RST_N;
 
 /* Port definitions */
 public:
  tUWide PORT__instruction_inputs_mem_data;
  tUWide PORT__data_inputs_mem_data;
  tUWide PORT_instruction_outputs_;
  tUWide PORT_data_outputs_;
 
 /* Publicly accessible definitions */
 public:
  tUInt8 DEF_wr_flush_everything_whas____d10;
  tUInt8 DEF_wr_flush_everything_wget____d11;
 
 /* Local definitions */
 private:
  tUInt64 DEF_v__h2210;
  tUInt64 DEF_v__h2060;
  tUInt8 DEF_ff_id_ie_first__8_BITS_145_TO_143___d86;
  tUInt64 DEF_v__h3790;
  tUInt64 DEF_v__h3753;
  tUInt64 DEF_v__h3318;
  tUInt64 DEF_v__h3121;
  tUInt64 DEF_v__h2944;
  tUInt64 DEF_v__h2907;
  tUInt64 DEF_v__h1991;
  tUInt64 DEF_v__h1951;
  tUInt64 DEF_v__h1650;
  tUInt64 DEF_v__h1538;
  tUInt64 DEF_v__h1497;
  tUInt64 DEF_v__h1344;
  tUWide DEF_fn_decoder___d46;
  tUWide DEF_alu_unit_inputs___d99;
  tUWide DEF_mem_unit_communicate_with_core___d209;
  tUWide DEF_ff_ie_imem_first____d207;
  tUWide DEF_register_file_output_to_decode_stage____d97;
  tUWide DEF_ff_id_ie_first____d78;
  tUWide DEF_mem_unit_data_to_memory____d247;
  tUWide DEF_rg_data_to_instruction_memory_wget____d239;
  tUWide DEF_ff_if_id_first____d43;
  tUWide DEF_ff_imem_iwb_first____d228;
  tUWide DEF_wr_forward_from_MEM_wget____d193;
  tUWide DEF_rg_data_from_instruction_memory_1_wget____d2;
  tUWide DEF_rg_data_from_instruction_memory___d22;
  tUWide DEF_ff_ie_imem_first__07_BITS_336_TO_5___d226;
  tUWide DEF_alu_unit_inputs_9_BITS_331_TO_0___d115;
  tUWide DEF_mem_unit_data_to_memory__47_BITS_130_TO_0___d249;
  tUWide DEF_rg_data_to_instruction_memory_wget__39_BITS_13_ETC___d245;
  tUWide DEF_ff_id_ie_first__8_BITS_127_TO_0___d122;
  tUWide DEF_mem_unit_communicate_with_core_09_BITS_72_TO_0___d217;
  tUWide DEF_mem_unit_communicate_with_core_09_BITS_72_TO_4___d221;
  tUWide DEF_rg_data_from_instruction_memory_1_wget_BITS_65_ETC___d6;
  tUInt8 DEF_dest_addr__h2354;
  tUInt8 DEF__funct3__h2348;
  tUInt8 DEF_ff_id_ie_first__8_BIT_122___d85;
  tUWide DEF_IF_ff_id_ie_first__8_BITS_3_TO_0_09_EQ_0_10_TH_ETC___d132;
  tUWide DEF_IF_ff_id_ie_first__8_BITS_145_TO_143_6_EQ_0_6__ETC___d127;
  tUWide DEF_DONTCARE_CONCAT_1_CONCAT_DONTCARE_CONCAT_DONTC_ETC___d131;
  tUWide DEF_alu_unit_inputs_9_BITS_331_TO_0_15_CONCAT_IF_a_ETC___d120;
  tUWide DEF_register_file_output_to_decode_stage__7_BITS_1_ETC___d126;
  tUWide DEF_IF_NOT_rg_data_to_instruction_memory_whas__38__ETC___d246;
  tUWide DEF_IF_mem_unit_data_to_memory__47_BIT_131_48_THEN_ETC___d250;
  tUWide DEF_IF_ff_ie_imem_first__07_BIT_272_08_THEN_ff_ie__ETC___d218;
  tUWide DEF_ff_ie_imem_first__07_BITS_336_TO_273_12_CONCAT_ETC___d216;
  tUWide DEF_IF_ff_ie_imem_first__07_BIT_272_08_THEN_ff_ie__ETC___d222;
  tUWide DEF_ff_ie_imem_first__07_BITS_336_TO_273_12_CONCAT_ETC___d214;
  tUWide DEF_IF_rg_data_from_instruction_memory_1_wget_BIT__ETC___d7;
  tUWide DEF_IF_rg_data_from_instruction_memory_1_whas_THEN_ETC___d8;
  tUInt8 DEF_ff_id_ie_first__8_BITS_145_TO_143_6_EQ_4___d87;
  tUWide DEF__1_CONCAT_DONTCARE_CONCAT_DONTCARE_CONCAT_DONTC_ETC___d130;
  tUWide DEF__1_CONCAT_DONTCARE_CONCAT_DONTCARE_CONCAT_DONTC_ETC___d125;
  tUWide DEF_DONTCARE_CONCAT_DONTCARE_CONCAT_DONTCARE_CONCA_ETC___d124;
  tUWide DEF_DONTCARE_CONCAT_DONTCARE_CONCAT_DONTCARE_CONCA_ETC___d129;
  tUWide DEF_DONTCARE_CONCAT_ff_id_ie_first__8_BITS_127_TO__ETC___d123;
  tUWide DEF_DONTCARE_CONCAT_0_CONCAT_DONTCARE___d128;
  tUWide DEF_fn_decoder_6_CONCAT_ff_if_id_first__3_BITS_100_ETC___d54;
  tUWide DEF__1_CONCAT_IF_rg_data_from_instruction_memory_2__ETC___d27;
  tUWide DEF_rg_pc_4_CONCAT_rg_data_from_instruction_memory_ETC___d34;
  tUWide DEF_IF_ff_ie_imem_first__07_BIT_272_08_THEN_ff_ie__ETC___d220;
  tUWide DEF_IF_ff_ie_imem_first__07_BIT_272_08_THEN_ff_ie__ETC___d223;
  tUWide DEF_IF_wr_forward_from_MEM_whas__92_THEN_wr_forwar_ETC___d203;
  tUWide DEF__1_CONCAT_instruction_inputs_mem_data___d237;
  tUWide DEF_rg_data_from_instruction_memory_1_whas_AND_0_O_ETC___d9;
 
 /* Rules */
 public:
  void RL_rg_data_from_instruction_memory__dreg_update();
  void RL_rl_flush_stuff();
  void RL_rl_fetch();
  void RL_rl_decode();
  void RL_rl_operand_fetch();
  void RL_rl_execute();
  void RL_rl_forwarding_data_to_decode();
  void RL_rl_memory_stage();
  void RL_rl_write_back();
  void RL_rl_clock();
 
 /* Methods */
 public:
  tUInt8 METH_sout();
  tUInt8 METH_RDY_sout();
  void METH__instruction_inputs(tUWide ARG__instruction_inputs_mem_data);
  tUInt8 METH_RDY__instruction_inputs();
  tUWide METH_instruction_outputs_();
  tUInt8 METH_RDY_instruction_outputs_();
  void METH__data_inputs(tUWide ARG__data_inputs_mem_data);
  tUInt8 METH_RDY__data_inputs();
  tUWide METH_data_outputs_();
  tUInt8 METH_RDY_data_outputs_();
  tUInt8 METH_flush_from_cpu_();
  tUInt8 METH_RDY_flush_from_cpu_();
  void METH_sin(tUInt8 ARG_sin_in);
  tUInt8 METH_RDY_sin();
 
 /* Reset routines */
 public:
  void reset_RST_N(tUInt8 ARG_rst_in);
 
 /* Static handles to reset routines */
 public:
 
 /* Pointers to reset fns in parent module for asserting output resets */
 private:
 
 /* Functions for the parent module to register its reset fns */
 public:
 
 /* Functions to set the elaborated clock id */
 public:
  void set_clk_0(char const *s);
 
 /* State dumping routine */
 public:
  void dump_state(unsigned int indent);
 
 /* VCD dumping routines */
 public:
  unsigned int dump_VCD_defs(unsigned int levels);
  void dump_VCD(tVCDDumpType dt, unsigned int levels, MOD_mkriscv &backing);
  void vcd_defs(tVCDDumpType dt, MOD_mkriscv &backing);
  void vcd_prims(tVCDDumpType dt, MOD_mkriscv &backing);
  void vcd_submodules(tVCDDumpType dt, unsigned int levels, MOD_mkriscv &backing);
};

#endif /* ifndef __mkriscv_h__ */
