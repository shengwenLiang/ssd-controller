//
// Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
//
// On Tue Jul 19 13:19:26 IST 2016
//
//
// Ports:
// Name                         I/O  size props
// fn_slt                         O    64
// fn_slt__in1                    I    64
// fn_slt__in2                    I    64
// fn_slt__immediate              I    12
// fn_slt__imm_flag               I     1
//
// Combinational paths from inputs to outputs:
//   (fn_slt__in1, fn_slt__in2, fn_slt__immediate, fn_slt__imm_flag) -> fn_slt
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module module_fn_slt(fn_slt__in1,
		     fn_slt__in2,
		     fn_slt__immediate,
		     fn_slt__imm_flag,
		     fn_slt);
  // value method fn_slt
  input  [63 : 0] fn_slt__in1;
  input  [63 : 0] fn_slt__in2;
  input  [11 : 0] fn_slt__immediate;
  input  fn_slt__imm_flag;
  output [63 : 0] fn_slt;

  // signals for module outputs
  wire [63 : 0] fn_slt;

  // remaining internal signals
  wire [63 : 0] IF_fn_slt__imm_flag_THEN_SEXT_fn_slt__immediat_ETC___d3;
  wire fn_comparator___d9, x__h118;

  // value method fn_slt
  assign fn_slt =
	     (fn_slt__in1[63] &&
	      !IF_fn_slt__imm_flag_THEN_SEXT_fn_slt__immediat_ETC___d3[63] ||
	      ~x__h118 && fn_comparator___d9) ?
	       64'd1 :
	       64'd0 ;

  // remaining internal signals
  module_fn_comparator instance_fn_comparator_0(.fn_comparator__op1(fn_slt__in1),
						.fn_comparator__op2(IF_fn_slt__imm_flag_THEN_SEXT_fn_slt__immediat_ETC___d3),
						.fn_comparator(fn_comparator___d9));
  assign IF_fn_slt__imm_flag_THEN_SEXT_fn_slt__immediat_ETC___d3 =
	     fn_slt__imm_flag ?
	       { {52{fn_slt__immediate[11]}}, fn_slt__immediate } :
	       fn_slt__in2 ;
  assign x__h118 =
	     fn_slt__in1[63] ^
	     IF_fn_slt__imm_flag_THEN_SEXT_fn_slt__immediat_ETC___d3[63] ;
endmodule  // module_fn_slt

