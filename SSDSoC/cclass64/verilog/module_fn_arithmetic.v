//
// Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
//
// On Tue Jul 19 13:19:27 IST 2016
//
//
// Ports:
// Name                         I/O  size props
// fn_arithmetic                  O   133
// fn_arithmetic__opcode          I     5
// fn_arithmetic__funct3          I     3
// fn_arithmetic__funct7          I     7
// fn_arithmetic__operand1        I    64
// fn_arithmetic__operand2        I    64
// fn_arithmetic__immediate_value  I    20
// fn_arithmetic_program_counter  I    64
//
// Combinational paths from inputs to outputs:
//   (fn_arithmetic__opcode,
//    fn_arithmetic__funct3,
//    fn_arithmetic__funct7,
//    fn_arithmetic__operand1,
//    fn_arithmetic__operand2,
//    fn_arithmetic__immediate_value,
//    fn_arithmetic_program_counter) -> fn_arithmetic
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module module_fn_arithmetic(fn_arithmetic__opcode,
			    fn_arithmetic__funct3,
			    fn_arithmetic__funct7,
			    fn_arithmetic__operand1,
			    fn_arithmetic__operand2,
			    fn_arithmetic__immediate_value,
			    fn_arithmetic_program_counter,
			    fn_arithmetic);
  // value method fn_arithmetic
  input  [4 : 0] fn_arithmetic__opcode;
  input  [2 : 0] fn_arithmetic__funct3;
  input  [6 : 0] fn_arithmetic__funct7;
  input  [63 : 0] fn_arithmetic__operand1;
  input  [63 : 0] fn_arithmetic__operand2;
  input  [19 : 0] fn_arithmetic__immediate_value;
  input  [63 : 0] fn_arithmetic_program_counter;
  output [132 : 0] fn_arithmetic;

  // signals for module outputs
  wire [132 : 0] fn_arithmetic;

  // remaining internal signals
  reg [63 : 0] IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66,
	       alu_result___1__h175;
  wire [63 : 0] IF_fn_arithmetic__opcode_EQ_0b1110_OR_fn_arith_ETC___d67,
		SEXT_fn_addsub_3_BITS_31_TO_0_4___d35,
		SEXT_fn_arithmetic__immediate_value_BITS_11_TO_ETC___d47,
		SEXT_fn_arithmetic__immediate_value__q1,
		alu_result___1__h103,
		alu_result___1__h353,
		alu_result___1__h425,
		alu_result___1__h472,
		alu_result___1__h540,
		alu_result___1__h541,
		alu_result___1__h567,
		alu_result___1__h680,
		alu_result___1__h681,
		alu_result___1__h728,
		alu_result___1__h729,
		alu_result___1__h95,
		fn_addsub___d33,
		x__h25;
  wire [31 : 0] fn_addsub_3_BITS_31_TO_0__q2;
  wire [11 : 0] x__h732;
  wire x__h308;

  // value method fn_arithmetic
  assign fn_arithmetic =
	     { x__h25,
	       fn_arithmetic__opcode != 5'b0 &&
	       fn_arithmetic__opcode != 5'b01000,
	       fn_arithmetic__operand2,
	       fn_arithmetic__funct3[1:0],
	       fn_arithmetic__funct3[2],
	       fn_arithmetic__opcode == 5'b01000 } ;

  // remaining internal signals
  module_fn_sll instance_fn_sll_1(.fn_sll__in1(fn_arithmetic__operand1),
				  .fn_sll__in2(fn_arithmetic__operand2),
				  .fn_sll__immediate(x__h732),
				  .fn_sll__imm_flag(~fn_arithmetic__opcode[3]),
				  .fn_sll_op_32(fn_arithmetic__opcode[1]),
				  .fn_sll(alu_result___1__h353));
  module_fn_slt instance_fn_slt_2(.fn_slt__in1(fn_arithmetic__operand1),
				  .fn_slt__in2(fn_arithmetic__operand2),
				  .fn_slt__immediate(x__h732),
				  .fn_slt__imm_flag(~fn_arithmetic__opcode[3]),
				  .fn_slt(alu_result___1__h425));
  module_fn_sltu instance_fn_sltu_3(.fn_sltu__in1(fn_arithmetic__operand1),
				    .fn_sltu__in2(fn_arithmetic__operand2),
				    .fn_sltu__immediate(x__h732),
				    .fn_sltu__imm_flag(~fn_arithmetic__opcode[3]),
				    .fn_sltu(alu_result___1__h472));
  module_fn_sra_srl instance_fn_sra_srl_4(.fn_sra_srl__in1(fn_arithmetic__operand1),
					  .fn_sra_srl__in2(fn_arithmetic__operand2),
					  .fn_sra_srl__immediate(x__h732),
					  .fn_sra_srl_rl_ra_flag(fn_arithmetic__funct7[5]),
					  .fn_sra_srl__imm_flag(~fn_arithmetic__opcode[3]),
					  .fn_sra_srl_op_32(fn_arithmetic__opcode[1]),
					  .fn_sra_srl(alu_result___1__h567));
  module_fn_addsub instance_fn_addsub_0(.fn_addsub__in1(fn_arithmetic__operand1),
					.fn_addsub__in2(fn_arithmetic__operand2),
					.fn_addsub__immediate(x__h732),
					.fn_addsub__sub_flag(fn_arithmetic__opcode ==
							     5'b01100 &&
							     fn_arithmetic__funct3 ==
							     3'b0 &&
							     fn_arithmetic__funct7[5] ||
							     fn_arithmetic__opcode ==
							     5'b01110 &&
							     fn_arithmetic__funct3 ==
							     3'b0 &&
							     fn_arithmetic__funct7[5]),
					.fn_addsub__imm_flag(~x__h308),
					.fn_addsub(fn_addsub___d33));
  assign IF_fn_arithmetic__opcode_EQ_0b1110_OR_fn_arith_ETC___d67 =
	     ((fn_arithmetic__opcode == 5'b01110 ||
	       fn_arithmetic__opcode == 5'b00110) &&
	      fn_arithmetic__funct3 == 3'b0 ||
	      fn_arithmetic__opcode == 5'b0 ||
	      fn_arithmetic__opcode == 5'b01000 ||
	      (fn_arithmetic__opcode == 5'b00100 ||
	       fn_arithmetic__opcode == 5'b01100) &&
	      fn_arithmetic__funct3 == 3'b0) ?
	       alu_result___1__h175 :
	       IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 ;
  assign SEXT_fn_addsub_3_BITS_31_TO_0_4___d35 =
	     { {32{fn_addsub_3_BITS_31_TO_0__q2[31]}},
	       fn_addsub_3_BITS_31_TO_0__q2 } ;
  assign SEXT_fn_arithmetic__immediate_value_BITS_11_TO_ETC___d47 =
	     { {52{x__h732[11]}}, x__h732 } ;
  assign SEXT_fn_arithmetic__immediate_value__q1 =
	     { {44{fn_arithmetic__immediate_value[19]}},
	       fn_arithmetic__immediate_value } ;
  assign alu_result___1__h103 =
	     alu_result___1__h95 + fn_arithmetic_program_counter ;
  assign alu_result___1__h540 =
	     fn_arithmetic__operand1 ^ fn_arithmetic__operand2 ;
  assign alu_result___1__h541 =
	     fn_arithmetic__operand1 ^
	     SEXT_fn_arithmetic__immediate_value_BITS_11_TO_ETC___d47 ;
  assign alu_result___1__h680 =
	     fn_arithmetic__operand1 | fn_arithmetic__operand2 ;
  assign alu_result___1__h681 =
	     fn_arithmetic__operand1 |
	     SEXT_fn_arithmetic__immediate_value_BITS_11_TO_ETC___d47 ;
  assign alu_result___1__h728 =
	     fn_arithmetic__operand1 & fn_arithmetic__operand2 ;
  assign alu_result___1__h729 =
	     fn_arithmetic__operand1 &
	     SEXT_fn_arithmetic__immediate_value_BITS_11_TO_ETC___d47 ;
  assign alu_result___1__h95 =
	     { SEXT_fn_arithmetic__immediate_value__q1[51:0], 12'd0 } ;
  assign fn_addsub_3_BITS_31_TO_0__q2 = fn_addsub___d33[31:0] ;
  assign x__h25 =
	     fn_arithmetic__opcode[0] ?
	       (fn_arithmetic__opcode[3] ?
		  alu_result___1__h95 :
		  alu_result___1__h103) :
	       IF_fn_arithmetic__opcode_EQ_0b1110_OR_fn_arith_ETC___d67 ;
  assign x__h308 = fn_arithmetic__opcode[3] & fn_arithmetic__opcode[2] ;
  assign x__h732 = fn_arithmetic__immediate_value[11:0] ;
  always@(fn_arithmetic__opcode or
	  fn_addsub___d33 or SEXT_fn_addsub_3_BITS_31_TO_0_4___d35)
  begin
    case (fn_arithmetic__opcode)
      5'b00110, 5'b01110:
	  alu_result___1__h175 = SEXT_fn_addsub_3_BITS_31_TO_0_4___d35;
      default: alu_result___1__h175 = fn_addsub___d33;
    endcase
  end
  always@(fn_arithmetic__funct3 or
	  alu_result___1__h353 or
	  alu_result___1__h425 or
	  alu_result___1__h472 or
	  fn_arithmetic__opcode or
	  alu_result___1__h540 or
	  alu_result___1__h541 or
	  alu_result___1__h567 or
	  alu_result___1__h680 or
	  alu_result___1__h681 or
	  alu_result___1__h728 or alu_result___1__h729)
  begin
    case (fn_arithmetic__funct3)
      3'd0: IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 = 64'd0;
      3'b001:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      alu_result___1__h353;
      3'b010:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      alu_result___1__h425;
      3'b011:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      alu_result___1__h472;
      3'b100:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      fn_arithmetic__opcode[3] ?
		alu_result___1__h540 :
		alu_result___1__h541;
      3'b101:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      alu_result___1__h567;
      3'b110:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      fn_arithmetic__opcode[3] ?
		alu_result___1__h680 :
		alu_result___1__h681;
      3'b111:
	  IF_fn_arithmetic__funct3_EQ_0b1_7_THEN_fn_sll__ETC___d66 =
	      fn_arithmetic__opcode[3] ?
		alu_result___1__h728 :
		alu_result___1__h729;
    endcase
  end
endmodule  // module_fn_arithmetic

