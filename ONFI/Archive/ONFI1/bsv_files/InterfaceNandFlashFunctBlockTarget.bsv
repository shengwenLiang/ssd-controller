package InterfaceNandFlashFunctBlockTarget ;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		ONFi - TARGET INTERFACE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface TargetInterface   ;
 
	method Bit#(8) _data_to_nfc_m ( ) ;
	method Action _data_from_nfc_m ( Bit#(8) _data_from_nfc ) ;
	method Action _onfi_ce_n_m ( bit _onfi_ce_n ) ;
	method Action _onfi_we_n_m ( bit _onfi_we_n ) ;
	method Action _onfi_re_n_m ( bit _onfi_re_n ) ;
	method Action _onfi_wp_n_m ( bit _onfi_wp_n ) ;
	method Action _onfi_cle_m ( bit _onfi_cle ) ;
	method Action _onfi_ale_m ( bit _onfi_ale ) ;
	method bit t_ready_busy_n_ ;

endinterface

function TargetInterface fn_onfi_target_interface ( Wire#(bit) wr_onfi_ce_n, Wire#(bit) wr_onfi_cle, Wire#(bit) wr_onfi_ale, Wire#(bit) wr_onfi_we_n, Wire#(bit) wr_onfi_re_n, Wire#(bit) wr_onfi_wp_n, Reg#(Bit#(8)) rg_data_to_nfc, Wire#(Bit#(8)) wr_data_from_nfc, Reg#(bit) rg_t_ready_busy_n) ;

	return ( interface TargetInterface ;

				method Action _data_from_nfc_m ( _data_from_nfc ) ;
					wr_data_from_nfc <= _data_from_nfc ;
				endmethod

				method Bit#(8) _data_to_nfc_m ( ) ;
					return rg_data_to_nfc ;
				endmethod

				method Action _onfi_ce_n_m ( _onfi_ce_n ) ;
					wr_onfi_ce_n <= _onfi_ce_n ;
				endmethod

				method Action _onfi_we_n_m ( _onfi_we_n ) ;
					wr_onfi_we_n <= _onfi_we_n ;
				endmethod

				method Action _onfi_re_n_m ( _onfi_re_n ) ;
					wr_onfi_re_n <= _onfi_re_n ;
				endmethod

				method Action _onfi_wp_n_m ( _onfi_wp_n ) ;
					wr_onfi_wp_n <= _onfi_wp_n ;
				endmethod

				method Action _onfi_cle_m ( _onfi_cle ) ;
					wr_onfi_cle <= _onfi_cle ;
				endmethod

				method Action _onfi_ale_m ( _onfi_ale ) ;
					wr_onfi_ale <= _onfi_ale ;
				endmethod

				method bit t_ready_busy_n_ ( ) ;
					return rg_t_ready_busy_n ;
				endmethod

				endinterface ) ;

endfunction

endpackage
