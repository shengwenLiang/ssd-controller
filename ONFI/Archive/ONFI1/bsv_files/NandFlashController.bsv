/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
/* For the NVM the target will look like -> 4kB page. 256 pages per block and 2048 blocks (total 19 addr bits)
   Assuming that NVM sends address in the following fashion (MSB to LSB) : {Block_Addr (11bits), page_addr(8 bits) }
    
   Actually target has 2 chips. Each chip has 2 LUNs.Each lun has 2 plane . Each plane has 1024 blocks. Each block has 64 pages(Total of 16Gb).
   */

`include "global_parameters_Flash.bsv"

package NandFlashController ;

import Vector::*;
import BRAM :: * ;
import FIFO::*;
import FIFOF::*;
import BRAMFIFO::*;
import DReg::*;
import InterfaceNandFlashController :: * ;

interface NFC_Interface ;
	interface NandFlashInterface nvm_nfc_interface  ;
	interface ONFiInterface nfc_onfi_interface ;
endinterface

//Creating enumeration for variois states in READ/PROGRAM/ERASE/RESET ... target
typedef enum {
	IDLE ,
	PROGRAM_PAGE ,
	PROGRAM_PAGE_CACHE ,
	PROGRAM_PAGE_MULTI_PLANE 
} Program_states deriving( Bits, Eq ) ;

typedef enum {
	IDLE ,
	READ_PAGE ,
	READ_PAGE_MULTI_PLANE 
} Read_states deriving( Bits, Eq ) ;

typedef enum {
	ASK_STATUS1 ,
	ASK_STATUS2 ,
	P_ADDR_S ,
	B_ADDR_S ,
	L_ADDR_S ,
	ENABLE_S_READ1 ,
	ENABLE_S_READ2 ,
	ENABLE_S_READ3 ,
	WAIT1 ,
	WAIT2 ,
	WAIT3 ,
	WAIT4 ,
	WAIT5 ,
	READ_STATUS1 ,
	READ_STATUS2 ,
	READ_STATUS3 ,
	FINISH_STATUS ,
	START_PROGRAM ,
	C_ADDR ,
	C_ADDR1 ,
	C_ADDR_1 ,
	C_ADDR_2 ,
	P_ADDR ,
	P_ADDR_1 ,
	B_ADDR ,
	B_ADDR_1 ,
	L_ADDR ,
	L_ADDR_1 ,
	START_DATA ,
	START_READ ,
	START_READ_C ,
	CONT_READ ,
	START_ERASE ,
	END_COMMAND ,
	END_COMMAND_1 ,
	SELECT_C_R ,
	DUMMY 
} Operation_state deriving( Bits, Eq ) ;


(* synthesize *)	 
module mkNandFlashController ( NFC_Interface ) ;
// Wires and Regs related to the Nand Flash Interface 
	Wire#(Bit#(64))	        wr_address_from_nvm     <- mkDWire(0) ;	// address
	Wire#(Bit#(`WDC))	wr_data_from_nvm 	<- mkDWire(0) ;	// data in
	Reg#(Bit#(`WDC))	rg_data_to_nvm 		<- mkReg(0) ;	// data out
	Wire#(Bit#(1))		wr_nand_ce_n		<- mkDWire(1) ;	// active low
	Wire#(Bit#(1))		wr_nand_we_n		<- mkDWire(1) ;	// active low
	Wire#(Bit#(1))		wr_nand_re_n		<- mkDWire(1) ;	// active low
	Reg#(Bit#(1))		rg_interrupt		<- mkDReg(0) ;	// active high
	Reg#(Bit#(1))		rg_ready_busy_n		<- mkReg(1) ;	// active low
	Wire#(Bit#(11))		wr_w_length		<- mkDWire(0) ;	//Write length
	Wire#(Bit#(11))		wr_r_length		<- mkDWire(0) ;	//Write length
	Wire#(Bit#(1))		wr_nand_erase           <- mkDWire(0) ;	//Write length
	Reg#(Bit#(1))		rg_write_success	<- mkDReg(0) ;	// active high
	Reg#(Bit#(1))		rg_write_fail		<- mkDReg(0) ;	// active high
	Reg#(Bit#(1))		rg_erase_success	<- mkDReg(0) ;	// active high
	Reg#(Bit#(1))		rg_erase_fail		<- mkDReg(0) ;	// active high
	
// Regs and Wires for ONFI Interface
	Reg#(Bit#(8))          	    	    rg_data_to_flash	    <- mkReg(0) ;     		 // data out 
	Wire#(Bit#(8))         	    	    wr_data_from_flash      <- mkWire ; 		 // data in  
	Reg#(Bit#(1))			    rg_onfi_we_n	    <- mkReg(1) ;   		 // active low
	Reg#(Bit#(1))			    rg_onfi_re_n	    <- mkReg(1) ;   		 // active low
	Reg#(Bit#(1))			    rg_onfi_cle 	    <- mkReg(0) ;   		 // active high
	Reg#(Bit#(1))			    rg_onfi_ale		    <- mkReg(0) ;   		 // active high
	Reg#(Bit#(1))			    rg_onfi_wp_n	    <- mkReg(1) ;   		 // active low
	Vector#(`TOTAL_CHIPS,Wire#(Bit#(1))) wr_ready_busy_n	    <- replicateM(mkWire) ;   // active low
	Vector#(`TOTAL_CHIPS,Reg#(Bit#(1))) rg_onfi_ce_n	    <- replicateM(mkReg(1)) ;    // active low
	
//Other Regs , Wires and FIFO's
	//Will use  FIFO's for recieving data b/t NVMe and NFC..
	FIFOF#(Bit#(`WDC))                  data_w_fifo   <- mkUGSizedFIFOF(`FIFO_ROWS+1);	
	//Will use  FIFO's for sending data b/t NVMe and NFC.
	FIFOF#(Bit#(`WDC))                  data_r_fifo   <- mkUGSizedFIFOF(`FIFO_ROWS+1);	
	Reg#(Bit#(1))                  data_w_fifo_free   <- mkReg(1); // Initially both write FIFO are free.
	Reg#(Bit#(1))                  data_r_fifo_free   <- mkReg(1); // Initially both read FIFO are free.
	Reg#(Bit#(64))                    addr_register   <- mkReg(0);             // Address register to store address.
	Reg#(Bit#(64))                 present_nvm_addr   <- mkReg(0); 
	Reg#(Bit#(64))                    next_nvm_addr   <- mkReg(0);             // Tracking Address from nvm with the length parameter.
 Reg#(Bit#(TLog#(TAdd#(`FIFO_ROWS,1))))   q_data_count_t  <- mkReg(0); // Track the data being put into the FIFO from NVM(_t means take) 
 Reg#(Bit#(TLog#(TAdd#(`FIFO_ROWS,1))))   q_data_count_g  <- mkReg(0); // Track the data being put into the FIFO from NVM(_g means give)
	Reg#(Bit#(11))		          local_length_w  <- mkReg(0) ;	           // This will hold the length parameter during the first request and then tracks(for write)
	Reg#(Bit#(11))		          local_length_r  <- mkReg(0) ;	           // This will hold the length parameter during the first request and then tracks(for read)
	Reg#(Bit#(11))		         pages2b_written  <- mkReg(0) ;	           // This will hold the length parameter during the first request and then tracks(for write)
	Reg#(Bit#(1))                          chip_sel   <- mkReg(0);             // Points to which chip is going to be selected.
	Reg#(Bit#(1))                         plane_sel   <- mkReg(0);             // Points to which plane is going to be selected.
	Reg#(Bit#(1))                     cache_op_need   <- mkReg(0);             // Flag to indicate whether cache operation is needed
	Reg#(Bit#(8))		             addr_cycl1   <- mkReg(0) ;// Can process 2 requests of write at a time, since we have 2 FIFO taking data from NVM
	Reg#(Bit#(8))		             addr_cycl2   <- mkReg(0) ;
	Reg#(Bit#(8))		             addr_cycl3   <- mkReg(0) ;
	Reg#(Bit#(8))		             addr_cycl4   <- mkReg(0) ;
	Reg#(Bit#(8))                        addr_cycl5   <- mkReg(0) ;
	Reg#(Bit#(8))	                  a_cycl3_buff1   <- mkReg(0) ;		// Temp location for addr
	Reg#(Bit#(8))	                  a_cycl3_buff2   <- mkReg(0) ;		// Temp location for addr
	Reg#(Bit#(8))	                  a_cycl4_buff1   <- mkReg(0) ;		// Temp location for  addr
	Reg#(Bit#(8))	                  a_cycl4_buff2   <- mkReg(0) ;		// Temp location for  addr
	Reg#(Bit#(8))	                  a_cycl5_buff1   <- mkReg(0) ;		// Temp location for  addr
	Reg#(Bit#(8))	                  a_cycl5_buff2   <- mkReg(0) ;		// Temp location for  addr
	Reg#(Bit#(8))	             mplane_cycl3_buff1   <- mkReg(0) ;		// Temp location for multi plane addr
	Reg#(Bit#(8))	             mplane_cycl3_buff2   <- mkReg(0) ;
	Reg#(Bit#(8))                mplane_cycl4_buff1   <- mkReg(0) ; 
	Reg#(Bit#(8))                mplane_cycl4_buff2   <- mkReg(0) ; 
	Reg#(Bit#(8))                mplane_cycl5_buff1   <- mkReg(0) ; 
	Reg#(Bit#(8))                mplane_cycl5_buff2   <- mkReg(0) ; 
	Reg#(Bit#(TLog#(TDiv#(`WDC,8))))     byte_count   <- mkReg(0) ;            // This is to keep track of how many bytes are sent to flash.
	Reg#(Bit#(TLog#(TDiv#(`WDC,8))))     zero_index   <- mkReg(0) ;            // Index to compute filling of zeros in case of col offset.
	Reg#(Bit#(1))                         new_r_req   <- mkReg(0);             // New read request flag.
	Reg#(Bit#(`WDC))	        data_from_flash   <- mkReg(0) ;	// Buffer to store half words recieved from flash and convert them to `WDC to send to nvm.
	Reg#(Bit#(1))                        reset_flag   <- mkReg(1);             // To reset the flash memory for power-on-reset.
	Reg#(Bit#(1))                     reset_ongoing   <- mkReg(1);             
	Reg#(Bit#(1))                     reset_applied   <- mkReg(1);             
	Reg#(Bit#(`COLUMN_WIDTH))          col_offset_p   <- mkReg(0) ;            // Col offset during program
	Reg#(Bit#(`COLUMN_WIDTH))          col_offset_r   <- mkReg(0) ;            // Col offset during read
	Reg#(Bit#(`COLUMN_WIDTH))      buf_col_offset_r   <- mkReg(0) ;            // Col offset during read
	Reg#(Bit#(1))                     get_next_addr   <- mkReg(0) ;
	Reg#(Bit#(1))               block_erase_ongoing   <- mkReg(0) ;            // Indicates erase is in progress
	Reg#(Bit#(1))                      read_pending   <- mkReg(0) ;            // Indicates read in progress
	Reg#(Bit#(1))                     start_program   <- mkReg(0) ;            // Indicates start of program cycle
	Reg#(Bit#(1))                        last_r_req   <- mkReg(0) ;            // Indicates last read in progress
	Reg#(Bit#(1))                stay_with_decision   <- mkReg(0) ;            // Control for next flag "decide_read"
	Reg#(Bit#(1))                       decide_read   <- mkReg(0) ;            // Indicates decision of read
	Reg#(Bit#(1))                 initial_status_ck   <- mkReg(0) ;            // Indicates whether a status check is needed or not.
	Reg#(Bit#(1))                      q_fill_zeros   <- mkReg(0) ;            // Indicates whether Q needs zeros in case of col offset.
	Reg#(Bit#(1))              multi_plane_r_pend_1   <- mkReg(0) ;            // Multi plane op flag
	Reg#(Bit#(1))              multi_plane_r_pend_2   <- mkReg(0) ;            // Multi plane op flag
	Reg#(Bit#(1))                     page_r_pend_1   <- mkReg(0) ;            // Multi plane op flag
	Reg#(Bit#(1))                     page_r_pend_2   <- mkReg(0) ;
	Reg#(Bit#(1))            multi_plane_after_page   <- mkReg(0) ;            // To identify muti plane req after a single page request
	Reg#(Bit#(2))                      status_count   <- mkReg(0) ; 
	Reg#(Bit#(2))                       status_done   <- mkReg(0) ;            // Needed to keep track of the status checks done during program.
	Reg#(Bit#(2))                         erase_cnt   <- mkReg(0) ; 
	Reg#(Bit#(1))                     flag_end_read   <- mkReg(0) ;
	Reg#(Bit#(1))                   data_reg_loaded   <- mkReg(0) ;
	Reg#(Bit#(1))                       first_entry   <- mkReg(0) ;
	Reg#(Bit#(1))                    program_failed   <- mkReg(0) ;            //To keep track if any program failed.
	Reg#(Bit#(1))                     last_status_r   <- mkReg(0) ;            //To keep track of the last status read of the request
	Reg#(Bit#(1))                        first_byte   <- mkReg(0) ;
	Reg#(Bit#(1))                     allow_write_q   <- mkReg(0) ;
	Reg#(Bit#(1))                    cal_block_addr   <- mkReg(0) ;
	Reg#(Operation_state)               erase_state   <- mkReg(ASK_STATUS1);   // State variable for erase operation.
	Reg#(Operation_state)                read_state   <- mkReg(ASK_STATUS1);   // State variable for read operation.     
	Reg#(Operation_state)             program_state   <- mkReg(ASK_STATUS1);   // State variable for write operation.  
	Reg#(Read_states)               present_r_state   <- mkReg(IDLE);          // Present read state.
	Reg#(Read_states)                  next_r_state   <- mkReg(IDLE);          // Next read state.
	Reg#(Program_states)            present_w_state   <- mkReg(IDLE);          // Present write state.
	Reg#(Program_states)               next_w_state   <- mkReg(IDLE);          // Next write state.
	Reg#(Program_states)               prev_w_state   <- mkReg(IDLE);          // Previous write state.
	//Below 4 registers are used for block erase operation.
	Vector#(`TOTAL_PLANE,Reg#(Bit#(8)))  erase_addr_cycl3     <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(8)))  erase_addr_cycl4     <- replicateM(mkReg(0)) ;
	Vector#(`TOTAL_PLANE,Reg#(Bit#(8)))  erase_addr_cycl5     <- replicateM(mkReg(0)) ;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	rule rl_standby_onfi (wr_nand_ce_n == 1'b1) ;
		//$display (" CHIP ENABLE PIN IS DISABLED NFC \n")   ;
	endrule


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////                                            Function to map address to physical locations on chip                                                /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* Here need to map to physical address in target for high band width	*/
function Bit#(64) fn_map_address (Bit#(64) addr_from_nvm);
Bit#(TSub#(`BLOCK_WIDTH,`LUN_WIDTH)) partial_addr1 = addr_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH:`COLUMN_WIDTH+`PAGE_WIDTH+`PLANE_WIDTH+`LUN_WIDTH+1] ;
Bit#(1) partial_addr2 = (addr_from_nvm[`COLUMN_WIDTH]^addr_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`LUN_WIDTH+1]);
Bit#(`PAGE_WIDTH) partial_addr3 = addr_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`LUN_WIDTH:`COLUMN_WIDTH+`LUN_WIDTH+1] ;
Bit#(`COLUMN_WIDTH) partial_addr4 = addr_from_nvm[`COLUMN_WIDTH-1:0] ;
return({'h0,addr_from_nvm[`COLUMN_WIDTH+`LUN_WIDTH],partial_addr1,partial_addr2,addr_from_nvm[`COLUMN_WIDTH],partial_addr3,partial_addr4});
endfunction

//Funtion to increment page address by 1.
function Bit#(64) fn_get_next_nvm_addr (Bit#(64) addr_from_nvm);
Bit#(TAdd#(TAdd#(TAdd#(`PAGE_WIDTH,`BLOCK_WIDTH),`PLANE_WIDTH),`LUN_WIDTH)) partial_addr5 = addr_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH-1:`COLUMN_WIDTH]+'h1;
Bit#(`COLUMN_WIDTH) partial_addr6 = addr_from_nvm[`COLUMN_WIDTH-1:0] ;
return({'h0,partial_addr5,partial_addr6}); 
endfunction

(* mutually_exclusive = "rl_reset, rl_send_data_to_nvm, rl_get_addr_from_nvm_for_read, rl_read_commands, rl_get_next_read_addr, rl_pgrm_commands, rl_q_data_from_nvme_fifo, rl_block_erase_initiate_get_address,rl_calculate_block_address, rl_block_erase_start " *)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                       Rules to take data from NVM to the FIFO while write                                                  ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	/*Put data from NVMe into the write queue */
	/*Our assumption is that for a given program command , the length does not cross over to the next block. i.e for a given program command,given A block address, the
	length can go from 0 to PAGES-1 */
	//If a program fails at any instant need to discard everythinh, hence need this condition on rule
	rule rl_q_data_from_nvme_fifo (wr_nand_ce_n == 1'b0 && wr_nand_we_n == 1'b0 && (data_w_fifo_free == 1'b1 || allow_write_q == 1'b1) && program_failed == 1'b0) ;
		data_w_fifo.enq(wr_data_from_nvm) ;
		if(q_data_count_t == 'h0)
		begin
			if(local_length_w == 'h0)   //Corresponds to a new request from NVM
			begin
				local_length_w  <= wr_w_length ;  //Need the length to be stored only during the first request of a lengthy write.
				pages2b_written <= wr_w_length ;
				addr_register   <= fn_map_address(wr_address_from_nvm) ;
               			next_nvm_addr   <= fn_get_next_nvm_addr(wr_address_from_nvm); 
				/*Right now 2 chips are separated based on the MSB address bit. adjacent blocks can also be put in two different chips.
				Say block0 in chip0 and block1 in chip1 as block0.For this address mapping has to be changed.We will deal this later*/
				//MSB bit selects which chip is to be written into
				chip_sel        <= wr_address_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH+1];
				plane_sel       <= wr_address_from_nvm[`COLUMN_WIDTH] ; // All even pages lie in first plane of LUN and odd pages in second plane of LUN
				if(wr_w_length == 'h1)
					status_count <= 'h1 ;
				else if(wr_w_length == 'h2)
				begin
					if(wr_address_from_nvm[`COLUMN_WIDTH] == 'h0) //Even plane.
						status_count <= 'h1 ; //Need to check status of only one LUN before start of read.
					else
						status_count <= 'h2 ; //Need to check status of two different LUNS before start of read.
				end
				else
				status_count <= 'h2 ;
				col_offset_p    <= wr_address_from_nvm[`COLUMN_WIDTH-1:0] ;
				byte_count      <= wr_address_from_nvm[`LBPR-1:0];//This is for byte offset within col offset.
				//FIFO is WDC bits wide, hence col address offset needs to be translated into numb of rows in fifo.
				q_data_count_t  <= q_data_count_t + wr_address_from_nvm[`COLUMN_WIDTH-1:`LBPR] +'h1 ;
			end	
			else
			begin
				local_length_w  <= local_length_w - 'h1 ;
				addr_register   <= fn_map_address(next_nvm_addr) ; //Map the new address (which was stored in next_address reg in prev page write)
				//Next page address is current address + 1
               			next_nvm_addr   <= fn_get_next_nvm_addr(next_nvm_addr); 
				plane_sel       <= next_nvm_addr[`COLUMN_WIDTH] ;
				//Col offset is applicable only for the first page in case of mutiple page request.
				col_offset_p      <= 'h0 ;
				q_data_count_t  <= q_data_count_t + 'h1 ;
			end
		end
		else if(q_data_count_t < (`FIFO_ROWS-1))
			q_data_count_t <= q_data_count_t + 'h1 ;
		else if(q_data_count_t < `FIFO_ROWS)
		begin
			//Last but one data cycle, need to update data_w_fifo_free, since this is used to get ready signal and we dont want any delay in it
			q_data_count_t <= q_data_count_t + 'h1 ;
			data_w_fifo_free <= 1'b0 ;
			allow_write_q    <= 1'b1 ;
		end
		else
		begin
			q_data_count_t   <= 'h0;
			allow_write_q    <= 1'b0 ;
			start_program    <= 1'b1 ;
			if(col_offset_p == 'h0) //Need to reset col add since only first page must have column addr.
			begin
				addr_cycl1 <= 'h0 ;
				addr_cycl2 <= 'h0 ;
			end
			else
			begin
			`ifdef COLUMN_WIDTH_LT_8
				addr_cycl1	  <= {'h0,addr_register[`COLUMN_WIDTH-1:0]};
				addr_cycl2 	  <= 'h0 ;
			`elsif COLUMN_WIDTH_E_8 
				addr_cycl1	 <= addr_register[7:0];
				addr_cycl2	 <= 'h0 ;
			`else
				addr_cycl1       <= addr_register[7:0];
				Bit#(TSub#(16,`COLUMN_WIDTH)) fill_z = 'h0 ;
				addr_cycl2       <= {fill_z,addr_register[`COLUMN_WIDTH-1:8]} ;//``COLUMN_WIDTH cannot be 16.Must be LTE 15
			`endif
			end
			addr_cycl3	 <= addr_register[7+`COLUMN_WIDTH:`COLUMN_WIDTH];
			addr_cycl4	 <= addr_register[15+`COLUMN_WIDTH:`COLUMN_WIDTH+8];
			addr_cycl5	 <= addr_register[23+`COLUMN_WIDTH:`COLUMN_WIDTH+16];
		end
	endrule
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                          Rule to create state machine for write to target                                                  ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/*Implementing plane first and LUN next states*/
	rule rl_write_state_decision (data_w_fifo_free == 1'b0 && present_w_state == IDLE) ;
		case(local_length_w)
			'h0     :	begin
						$display("Write Length parameter is zero");
					end
			'h1 	: 	begin
					/*If there is only one page to be written it is either the only page request from NVM or it can be any odd page request 
					(like 5,9 etc). Hence it can mean PROGRAM PAGE (only one page) or PROGRAM_PAGE_CACHE (5,9.. pages)*/
						if(cache_op_need == 1'b0)
							present_w_state <= PROGRAM_PAGE ;
						else
							present_w_state <= PROGRAM_PAGE_CACHE ;
						next_w_state    <= IDLE ;
						cache_op_need <= 1'b0 ;
					end
		'h2,'h3,'h4	 : 	begin
					/*IF two pages to be written there are two possibilities. a) The pages fall on different LUNs(plane1 of LUN0 , plane0 of LUN1 or
					 plane1 of LUN1 and plane0 of LUN0) b) They fall on the same LUN */
						if(plane_sel == 1'b0)
						begin
							present_w_state <= PROGRAM_PAGE_MULTI_PLANE ;
							if(cache_op_need == 1'b0)
								next_w_state <= PROGRAM_PAGE ;
							else
								next_w_state <= PROGRAM_PAGE_CACHE ;
							if(local_length_w == 'h2)
								cache_op_need <= 1'b0 ;
						end
						else
						begin
							if(cache_op_need == 1'b0)
								present_w_state <= PROGRAM_PAGE ;
							else
								present_w_state <= PROGRAM_PAGE_CACHE ;
							next_w_state <= IDLE ;
						end
					end
		default 	: 	begin
					/*For any other case we go for program page cache operation*/
						if(plane_sel == 1'b0)
						begin
							present_w_state <= PROGRAM_PAGE_MULTI_PLANE ;
							next_w_state    <= PROGRAM_PAGE_CACHE ;
						end
						else
						begin
							present_w_state <= PROGRAM_PAGE_CACHE ;
							next_w_state    <= IDLE ;
						end
						cache_op_need <= 1'b1 ;
					end
		endcase
	endrule

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                              Rule to  PROGRAM                                                                              ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	rule rl_pgrm_commands (start_program == 1'b1 && (present_w_state == PROGRAM_PAGE || present_w_state == PROGRAM_PAGE_CACHE || present_w_state == PROGRAM_PAGE_MULTI_PLANE));
		case(program_state)
		         ASK_STATUS1 : 	begin
					   if(prev_w_state == PROGRAM_PAGE_MULTI_PLANE && last_status_r == 1'b0)
					   	program_state <= START_PROGRAM ;
					   else
					   begin
					   	/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h78 ;
						if(status_done <= 'h3)
							status_done             <= status_done + 'h1 ;
						program_state           <= P_ADDR_S ;
					   end  
				       	end
			 P_ADDR_S    :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(last_status_r == 1'b1)
							rg_data_to_flash        <= a_cycl3_buff1;
						else
							rg_data_to_flash        <= addr_cycl3 ;
						program_state           <= B_ADDR_S ;
					end
			 B_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(last_status_r == 1'b1)
							rg_data_to_flash        <= a_cycl4_buff1;
						else
							rg_data_to_flash        <= addr_cycl4 ;
						program_state           <= L_ADDR_S ;
					end
			L_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(last_status_r == 1'b1)
							rg_data_to_flash        <= a_cycl5_buff1;
						else
							rg_data_to_flash        <= addr_cycl5 ;
						program_state           <= ENABLE_S_READ1 ;
					end
		      ENABLE_S_READ1 :  begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						program_state           <= WAIT1 ;
					end
			 WAIT1       :   begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						program_state           <= READ_STATUS1 ;
					end
		        READ_STATUS1 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(last_status_r == 1'b1)
						begin
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
							begin
								program_state  <= DUMMY ;
								if(wr_data_from_flash[0] == 1'b1 || wr_data_from_flash[1] == 1'b1)
									program_failed <= 1'b1 ;
							end
							else
								program_state  <= ASK_STATUS2 ;
						end
						else
						begin
						if(present_w_state == PROGRAM_PAGE_CACHE)
						begin	
							if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of program page cache
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ;
						end
						/*If preceding the multi plane program there was a program page cache operation then we need to check only the RDY flag
						.Hence cache_op_need == 1 says earlier we had cache op(refer to rule state_decision)*/
						else if(present_w_state == PROGRAM_PAGE_MULTI_PLANE && (cache_op_need == 1'b1 || (cache_op_need == 1'b0 && next_w_state == PROGRAM_PAGE_CACHE)))
							if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of program page cache
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ;
						else
						begin 
						/*Here we did not have a cache operation , hence need to check both RDY and ARDY flags*/
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[0] == 1'b1 || wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ; 
						end
						end
					end
			ASK_STATUS2  :  begin
					/*If LUN is busy dont waste 6 cycles by polling using 78h instead use 70h for polling*/
						/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h70 ;
						program_state           <= ENABLE_S_READ2 ;
					end
		      ENABLE_S_READ2 :  begin
		      				rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						program_state           <= WAIT2 ; 
					end
			    WAIT2    :  begin
			    			rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						program_state           <= READ_STATUS2 ;
					end
		        READ_STATUS2 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(pages2b_written == 'h0 || last_status_r == 1'b1)//Final status checking after everything 
						begin
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
							begin
								program_state  <= DUMMY ;
								if(wr_data_from_flash[0] == 1'b1 || wr_data_from_flash[1] == 1'b1)
									program_failed <= 1'b1 ;
							end
							else
								program_state  <= ASK_STATUS2 ;
						end
						else
						begin
						if(present_w_state == PROGRAM_PAGE_CACHE)
						begin	
							if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of program page cache
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ;
						end
						/*If preceding the multi plane program there was a program page cache operation then we need to check only the RDY flag
						.Hence cache_op_need == 1 says earlier we had cache op(refer to rule state_decision)*/
						else if(present_w_state == PROGRAM_PAGE_MULTI_PLANE && (cache_op_need == 1'b1 || (cache_op_need == 1'b0 && next_w_state == PROGRAM_PAGE_CACHE)))
							if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of program page cache
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[0] == 1'b1 || wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ;
						else
						begin 
						/*Here we did not have a cache operation , hence need to check both RDY and ARDY flags*/
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
								if(status_done > status_count) //Need to check validity of prev write.
									if(wr_data_from_flash[0] == 1'b1 || wr_data_from_flash[1] == 1'b1)
									begin
										program_failed <= 1'b1 ;
										program_state  <= DUMMY ;
									end
									else
										program_state  <= START_PROGRAM ;
								else
									program_state  <= START_PROGRAM ;
							else
								program_state  <= ASK_STATUS2 ; 
						end
						end
					end
		       START_PROGRAM :	begin
		       				/*Send 80h command series from here*/	
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h80 ;
						program_state           <= C_ADDR ;
					end
			     C_ADDR  :  begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl1 ;
						program_state           <= C_ADDR1 ;
					end
		             C_ADDR1 :  begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl2 ;
						program_state           <= P_ADDR ;
					end
			     P_ADDR  :  begin
			     			/*Send page address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl3 ;
						program_state           <= B_ADDR ; 
					end
			     B_ADDR  :  begin
			     			/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl4 ;
						program_state           <= L_ADDR ; 
					end
			     L_ADDR  :  begin
			     			/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl5 ;
						program_state           <= START_DATA ; 
						q_data_count_g          <= truncate(col_offset_p>>`LBPR) ;
					end
			START_DATA   :  begin
					/*Not toggling the we_n pin as in the standard since we don't need an negedge in we_n to write, Because by default bluespec fires every
					cycle.So every cycle if we_n is low we do a write irrespective of negedge in we_n. Idea is we_n negedge was used as reference ,since
					it is asynchronous , but we already have a reference in bluespec every cycle, hence dont need to toggle we_n*/
						
						/*Need to send 8 bits of data to flash everytime from the actual WDC bit data*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						let data_to_flash       =  data_w_fifo.first ;
						Bit#(TLog#(`WDC)) index = {'h0,byte_count} ;
						rg_data_to_flash        <= data_to_flash[((index+'h1)*8-'h1):(index*8)] ;

						Bit#(TAdd#(TLog#(`WDC),1)) comp = {'h0,byte_count} ;
						if(q_data_count_g == `FIFO_ROWS && comp == ((`WDC/8)-1))
						begin
							program_state   <= END_COMMAND ;
							q_data_count_g <= 'h0 ;
							data_w_fifo.deq ;
							byte_count <= 0;
						end 
						else
						begin
							program_state  	        <= START_DATA ; 
							if(comp == ((`WDC/8)-1))
							begin
								q_data_count_g <= q_data_count_g + 'h1 ;
								data_w_fifo.deq ;
								byte_count <= 0;
							end
							else
								byte_count <= byte_count + 'h1 ;
						end

					end		
			END_COMMAND  : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						if(present_w_state == PROGRAM_PAGE_MULTI_PLANE)
							rg_data_to_flash        <= 'h11 ;
						else if(present_w_state == PROGRAM_PAGE_CACHE)
							rg_data_to_flash        <= 'h15 ;
						else
							rg_data_to_flash        <= 'h10 ;
						program_state           <= DUMMY ;
						pages2b_written         <= pages2b_written - 'h1 ;//1 page is sent completely here.
					end
			DUMMY	     :	begin
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						//col_offset_p            <= 'h0 ;//Reset col offset
						if(program_failed == 1'b1)
						begin
							status_count     <= 'h0 ;
							status_done      <= 'h0 ;
							local_length_w   <= 'h0 ;
							prev_w_state     <= IDLE ;
							present_w_state  <= IDLE ;
							next_w_state     <= IDLE ;
							data_w_fifo_free <= 1'b1 ;
							start_program    <= 1'b0 ;
							data_w_fifo.clear ;
							rg_write_fail  <= 1'b1 ;
							pages2b_written <= 'h0 ;
						end
						else
						begin
						      if((pages2b_written == 'h1 || pages2b_written == 'h2) && present_w_state != PROGRAM_PAGE_MULTI_PLANE)
							begin
								//Keep copy of present addresses.This is used to check the read pass/fail for last but one LUN.
								a_cycl3_buff1   <= addr_cycl3;
								a_cycl4_buff1   <= addr_cycl4;
								a_cycl5_buff1   <= addr_cycl5;
							end
								
							/*Clear the length value if it is the last write of the request*/
							if(local_length_w == 'h1 && pages2b_written == 'h0)//Indicates last page program
							begin
								local_length_w <= 'h0 ;
								status_count   <= status_count - 'h1 ;
								program_state  <= ASK_STATUS2 ;
							end
							else if(status_count == 'h1 && local_length_w == 'h0)
							begin
								status_count  <= 'h0;
								program_state <= ASK_STATUS1 ;
								last_status_r <= 1'b1 ;
							end
							else
							begin
								start_program    <= 1'b0 ;
								data_w_fifo_free <= 1'b1 ; //Free the Write FIFO
								prev_w_state                    <= present_w_state ;
								program_state                   <= ASK_STATUS1 ;
								last_status_r                   <= 1'b0 ;
								status_done                     <= 'h0 ;
								if(present_w_state == next_w_state) //This will happen once operations are finished on 1 LUN.
									present_w_state <= IDLE ;
								else
									present_w_state <= next_w_state ;
								if(local_length_w == 'h0)
									rg_write_success <= 1'h1 ;
							end
						end
					end
			default      :  begin
						/*Ideally control wont be here*/
						program_state      <= ASK_STATUS1 ;
					end
		endcase
	endrule
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                      Rules to take addr and length from NVM while read to NVM                                              ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	rule rl_get_addr_from_nvm_for_read (wr_nand_ce_n == 1'b0 && wr_nand_we_n == 1'b1 && wr_nand_re_n == 1'b0) ;
		local_length_r    <= wr_r_length ;  
               	next_nvm_addr     <= fn_get_next_nvm_addr(wr_address_from_nvm); 
		present_nvm_addr  <= wr_address_from_nvm; 
		addr_register     <= fn_map_address(wr_address_from_nvm) ;
		/*Right now 2 chips are separated based on the MSB address bit. adjacent blocks can also be put in two different chips.Depends on FTL.This case FTL gets 
		number of chips as 2.
		Say block0 in chip0 and block1 in chip1 as block0.For this address mapping has to be changed.We will deal this later*/
		chip_sel        <= wr_address_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH+1];//MSB bit selects which chip is to e written into
		//plane_sel       <= wr_address_from_nvm[`COLUMN_WIDTH] ;
		col_offset_r    <= wr_address_from_nvm[`COLUMN_WIDTH-1:0] ;
		byte_count      <= wr_address_from_nvm[`LBPR-1:0];//This is for byte offset within col offset.
		//This is to fill zeros in case of offset , so that no problem during enquing in FIFO while reading from flash
		Bit#(`LBPR) local_byte_count = wr_address_from_nvm[`LBPR-1:0] ;
		if(local_byte_count != 'h0)
			q_fill_zeros <= 1'b1 ;
			
		if(wr_r_length == 'h1)
			status_count <= 'h1 ;
		else if(wr_r_length == 'h2)
		begin
			if(wr_address_from_nvm[`COLUMN_WIDTH] == 'h0) //Even plane.
				status_count <= 'h1 ; //Need to check status of only one LUN before start of read.
			else
				status_count <= 'h2 ; //Need to check status of two different LUNS before start of read.
		end
		else
			status_count <= 'h2 ;
		new_r_req       <= 1'b1 ;
		get_next_addr   <= 1'b1 ;
		read_pending    <= 1'b1 ;
	endrule
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                          Rule to create state machine to read from target                                                  ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/*Calculate address for next cycle first i.e Map the new address (which was stored in next_address reg in prev page read)*/
	rule rl_get_next_read_addr (data_r_fifo_free == 1'b1 && get_next_addr == 1'b1) ;
		if(col_offset_r == 'h0) //Need to reset col add since only first page must have column addr.
		begin
			addr_cycl1 <= 'h0 ;
			addr_cycl2 <= 'h0 ;
		end
		else
		begin
		`ifdef COLUMN_WIDTH_LT_8
			addr_cycl1	 <= {'h0,addr_register[`COLUMN_WIDTH-1:0]};
			addr_cycl2	 <= 'h0 ;
		`elsif COLUMN_WIDTH_E_8 
			addr_cycl1	  <= addr_register[7:0];
			addr_cycl2	  <= 'h0 ;
		`else
			addr_cycl1       <= addr_register[7:0];
			Bit#(TSub#(16,`COLUMN_WIDTH)) fill_z = 'h0 ;
			addr_cycl2       <= {fill_z,addr_register[`COLUMN_WIDTH-1:8]} ;//``COLUMN_WIDTH cannot be 16.Must be LTE 15
		`endif
		end
		addr_cycl3			<= addr_register[7+`COLUMN_WIDTH:`COLUMN_WIDTH];
		addr_cycl4	 		<= addr_register[15+`COLUMN_WIDTH:`COLUMN_WIDTH+8];
		addr_cycl5	 		<= addr_register[23+`COLUMN_WIDTH:`COLUMN_WIDTH+16];
		plane_sel       		<= present_nvm_addr[`COLUMN_WIDTH] ; 
		present_nvm_addr		<= next_nvm_addr ;
		addr_register                   <= fn_map_address(next_nvm_addr) ; //Map the new address (which was stored in next_address reg in prev page read)
		next_nvm_addr                   <= fn_get_next_nvm_addr(next_nvm_addr);
		get_next_addr                   <= 1'b0 ;
		if(stay_with_decision == 1'b1)
		begin
			stay_with_decision <= 1'b0 ;
			decide_read        <= 1'b0 ;
		end
		else
			decide_read      		<= 1'b1 ;
	endrule
	
	/*Implementing plane first and LUN next states*/
	rule rl_read_state_decision (data_r_fifo_free == 1'b1 && present_r_state == IDLE && decide_read == 1'b1) ;
		decide_read        <= 1'b0 ;
		case(local_length_r)
			'h0 : 	begin
					$display("Read Length parameter is zero");
				end
			'h1 : 	begin
				/*We will not be bothered about cache operations in read, since we are spreading A block in 4 planes, and reading these 4 pages(as a single 
				block) can be done using concurrent plane read and the time needed will be same or better compared to cache op, hence we ignore cache op*/
					if(status_count == 'h1)
					begin
						read_state        <= ASK_STATUS1 ;
						initial_status_ck <= 1'b1 ;
						status_count      <= 'h0 ;
					end
					else
					begin
						read_state        <= START_READ ;
					end
					present_r_state   <= READ_PAGE ;
					next_r_state      <= IDLE ;
					last_r_req        <= 1'b1 ;
				end
		   default  :	begin
				/*IF two or more pages are to be read there are two possibilities. a) The pages fall on different LUNs(plane1 of LUN0 , plane0 of LUN1 or
					 plane1 of LUN1 and plane0 of LUN0) b) They fall on the same LUN */
					if(plane_sel == 1'b0)
					begin
						present_r_state <= READ_PAGE_MULTI_PLANE ;
						next_r_state    <= READ_PAGE ;
						if(local_length_r == 'h2)
						begin
							last_r_req      <= 1'b1 ;
						end
						else
						begin
							last_r_req      <= 1'b0 ;
							local_length_r  <= local_length_r - 'h2 ;
						end
					end
					else
					begin
						present_r_state   <= READ_PAGE ;
						next_r_state      <= IDLE ;
						local_length_r    <= local_length_r - 'h1 ;
					end
					if(status_count != 'h0)
					begin
						read_state        <= ASK_STATUS1 ;
						initial_status_ck <= 1'b1 ;
						status_count      <= status_count - 'h1 ;
					end
					else
						read_state        <= START_READ ;
				end
		endcase
	endrule
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////                                                        Rule to READ                                                                        ///////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	rule rl_read_commands ((present_r_state == READ_PAGE || present_r_state == READ_PAGE_MULTI_PLANE) && get_next_addr == 1'b0 && data_r_fifo_free == 1'b1);
		case(read_state)
			START_READ   : 	begin
						/*Send 00h comand series from here*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h00 ;
						read_state              <= C_ADDR ;
				     	end
			C_ADDR       : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl1 ;
						read_state              <= C_ADDR1 ;
				     	end
			C_ADDR1      : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl2;
						read_state              <= P_ADDR ;
				     	end
			P_ADDR	     : 	begin
						/*Send page address for read*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl3 ;
						read_state              <= B_ADDR ; 
				     	end
			B_ADDR	     : 	begin
						/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl4 ;
						read_state              <= L_ADDR ;
				     	end 
			L_ADDR	     : 	begin
						/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= addr_cycl5 ;
						read_state              <= END_COMMAND ;
				     	end 
			END_COMMAND : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						col_offset_r            <= 'h0 ; //Clear the col offset, since it is valid only for first page.
						buf_col_offset_r        <= col_offset_r ;//Keep copy of offset for the FIFO 
						if(present_r_state == READ_PAGE_MULTI_PLANE)
						begin
							rg_data_to_flash        <= 'h32 ;
							read_state              <= DUMMY ;
							if(multi_plane_r_pend_1 == 1'b0)
							begin
								multi_plane_r_pend_1 <= 1'b1 ;
								mplane_cycl3_buff1 <= addr_cycl3;
								mplane_cycl4_buff1 <= addr_cycl4;
								mplane_cycl5_buff1 <= addr_cycl5;
								if(page_r_pend_1 == 1'b1) //If before multi-plane op there is an individual read op raise flag
									multi_plane_after_page <= 1'b1 ;
								else
									multi_plane_after_page <= 1'b0 ;
							end
							else
							begin
								multi_plane_r_pend_2 <= 1'b1 ;
								mplane_cycl3_buff2 <= addr_cycl3;
								mplane_cycl4_buff2 <= addr_cycl4;
								mplane_cycl5_buff2 <= addr_cycl4;
							end
						end
						else   //READ_PAGE command
						begin
							rg_data_to_flash        <= 'h30 ;
							if(last_r_req == 1'b1) //If it is the last page ,then in next cycle we need to go to READ_MODE
							begin
								read_state      <= ASK_STATUS1 ;
								if(new_r_req == 1'b1) //There is only one page/multi-plane one page read request got.
								begin
									page_r_pend_1   <= 1'b1 ;
									a_cycl3_buff1   <= addr_cycl3;
									a_cycl4_buff1   <= addr_cycl4;
									a_cycl5_buff1   <= addr_cycl5;
								end
								else
								begin
									page_r_pend_2   <= 1'b1 ;
									a_cycl3_buff2   <= addr_cycl3;
									a_cycl4_buff2   <= addr_cycl4;
									a_cycl5_buff2   <= addr_cycl5;
								end
							end
							else if(new_r_req == 1'b1)
							begin
								read_state      <= DUMMY ; //For the first READ_PAGE command don't go to READ_MODE during next cycle.
								page_r_pend_1   <= 1'b1 ;
								a_cycl3_buff1   <= addr_cycl3;
								a_cycl4_buff1   <= addr_cycl4;
								a_cycl5_buff1   <= addr_cycl5;
							end
							else
							begin
								read_state      <= ASK_STATUS1 ;
								page_r_pend_2   <= 1'b1 ;
								a_cycl3_buff2   <= addr_cycl3;
								a_cycl4_buff2   <= addr_cycl4;
								a_cycl5_buff2   <= addr_cycl4;
							end
						end
					end
			ASK_STATUS1 : 	begin
					   	/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h78 ;
						read_state              <= P_ADDR_S ;
				       	end 
			 P_ADDR_S    :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(initial_status_ck == 1'b1)
							rg_data_to_flash        <= addr_cycl3 ;
						else if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b0) //If multi plane is not preceeded by page op
							rg_data_to_flash        <= mplane_cycl3_buff1 ;
						else
							rg_data_to_flash        <= a_cycl3_buff1 ;
						read_state              <= B_ADDR_S ;
					end
			 B_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(initial_status_ck == 1'b1)
							rg_data_to_flash        <= addr_cycl4 ;
						else if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b0)
							rg_data_to_flash        <= mplane_cycl4_buff1 ;
						else
							rg_data_to_flash        <= a_cycl4_buff1 ;
						read_state              <= L_ADDR_S ;
					end
			L_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(initial_status_ck == 1'b1)
							rg_data_to_flash        <= addr_cycl5 ;
						else if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b0)
							rg_data_to_flash        <= mplane_cycl5_buff1 ;
						else
							rg_data_to_flash        <= a_cycl5_buff1 ;
						read_state              <= ENABLE_S_READ1 ;
					end
		      ENABLE_S_READ1 :  begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= WAIT1 ;
					end
			 WAIT1       :   begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= READ_STATUS1 ;
					end
		        READ_STATUS1 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of read
						begin
							if(initial_status_ck == 1'b1)
							begin
								read_state        <= START_READ ;
								initial_status_ck <= 1'b0 ;
							end
							else
								read_state      <= START_READ_C ;
						end
						else
							read_state      <= ASK_STATUS2 ;
					end
			ASK_STATUS2  :  begin
					/*If LUN is busy dont waste 6 cycles by polling using 78h instead use 70h for polling*/
						/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h70 ;
						read_state              <= ENABLE_S_READ2 ;
					end
		      ENABLE_S_READ2 :  begin
		      				rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= WAIT2 ; 
					end
			    WAIT2    :  begin
			    			rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= READ_STATUS2 ;
					end
		        READ_STATUS2 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(wr_data_from_flash[6] == 1'b1) //check only RDY bit in case of read page cache
							if(initial_status_ck == 1'b1)
							begin
								read_state        <= START_READ ;
								initial_status_ck <= 1'b0 ;
							end
							else
							read_state      <= START_READ_C ; //C for CACHE
						else
							read_state      <= ASK_STATUS2 ;
					end
			START_READ_C :	begin //Send 00 command to start reading
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h00 ;//Sending 00h command
						read_state              <= WAIT3 ;
					end
			WAIT3        :  begin //Enable re_n and wait for 3 cycles
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= WAIT4 ;
					end
			WAIT4        :  begin //Enable re_n and wait for 3 cycles
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= WAIT5 ;
						q_data_count_t <= truncate(buf_col_offset_r>>`LBPR) ;
					end
			//This wait is because of 1 cycle delay in BRAM used for flash model.Can be removed for actual design
			WAIT5        :  begin //Enable re_n and wait for 3 cycles
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						read_state              <= CONT_READ ;
					end
			CONT_READ    :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						
						Bit#(TAdd#(TLog#(`WDC),1)) comp = {'h0,byte_count} ;
						Bit#(TLog#(`WDC))         index = {'h0,byte_count} ;
						Bit#(TLog#(`WDC))       z_index = {'h0,zero_index} ;
						if(q_data_count_t == `FIFO_ROWS && comp == ((`WDC/8)-1)) 
						begin
							read_state   <= DUMMY ;
							q_data_count_t  <= 'h0 ;
							buf_col_offset_r               <= 'h0 ; //Reset this since first page read from cache is done.
							q_data_count_g  <= truncate(buf_col_offset_r>>`LBPR) ; //Start putting out data in FIFO from this address.
							if(q_fill_zeros == 'h0)
								data_r_fifo.enq({wr_data_from_flash,data_from_flash[index*8-'h1:0]}) ;
							else
							begin
								if(data_reg_loaded == 1'b1)
							        begin
				       	                           data_r_fifo.enq({wr_data_from_flash,data_from_flash[index*8-'h1:(z_index*8)]}<<(z_index*8));
								     data_reg_loaded <= 1'b0 ; //Reset flag
								end
								else
								     data_r_fifo.enq({wr_data_from_flash,'h0});
								q_fill_zeros <= 1'b0 ;
							end
							byte_count    <= 'h0 ;
							first_entry   <= 'h0 ;
							first_byte    <= 'h0;
						end
						else
						begin
							read_state   <= CONT_READ ; 
							if(comp == ((`WDC/8)-1))
							begin
								q_data_count_t  <= q_data_count_t  + 'h1 ;
								if(q_fill_zeros == 'h0)
									data_r_fifo.enq({wr_data_from_flash,data_from_flash[index*8-'h1:0]}) ;
								else
								begin
								   if(data_reg_loaded == 1'b1)
								   begin
					                           data_r_fifo.enq({wr_data_from_flash,data_from_flash[index*8-'h1:(z_index*8)]}<<(z_index*8));
								     data_reg_loaded <= 1'b0 ; //Reset flag
								   end
								   else
								     data_r_fifo.enq({wr_data_from_flash,'h0});
								   q_fill_zeros <= 1'b0 ;
								end
								byte_count <= 'h0;
								first_byte <= 'h0;
							end
							else
							begin
								//Bit#(`WDC) inter_data = {'h0,(wr_data_from_flash<<(index*8))} ;
								Bit#(`WDC) inter_data = {'h0,wr_data_from_flash} ;
								if(first_byte == 1'b0)
								begin
									data_from_flash <= inter_data ;
									first_byte      <= 1'b1 ;
								end
								else	
									data_from_flash <= data_from_flash | (inter_data<<(index*8)) ;
								byte_count <= byte_count + 'h1 ;
								if(first_entry == 1'b0)
								begin
									if(q_fill_zeros == 1'b1)
									begin
										data_reg_loaded <= 1'b1 ;
										zero_index      <= byte_count ;
									end
									first_entry     <= 1'b1 ;
								end
							end
						end
					end
			SELECT_C_R   :  begin
						/*Send 06h comand series from here*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h06 ;
						read_state              <= C_ADDR_1 ;
				     	end
			C_ADDR_1     : 	begin
		/*Need to send column address here , in case of col offset. But read in our case happens for full page after first page in lengthy read, hence col addr is 0*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= 'h0 ;
						read_state              <= C_ADDR_2 ;
				     	end
			C_ADDR_2     : 	begin
		/*Need to send column address here , in case of col offset. But read in our case happens for full page after first page in lengthy read, hence col addr is 0*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= 'h0 ;
						read_state              <= P_ADDR_1 ;
				     	end
			P_ADDR_1     : 	begin
						/*Send page address for read*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= a_cycl3_buff1 ;
						read_state              <= B_ADDR_1 ; 
				     	end
			B_ADDR_1     : 	begin
						/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= a_cycl4_buff1 ;
						read_state              <= L_ADDR_1 ;
				     	end
			L_ADDR_1     : 	begin
						/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						rg_data_to_flash        <= a_cycl5_buff1 ;
						read_state              <= END_COMMAND_1 ;
				     	end
			END_COMMAND_1 : begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'hE0 ; //End the command and start getting data.
						read_state              <= WAIT3 ;
					end
			DUMMY	     :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						col_offset_r            <= 'h0 ;
						buf_col_offset_r        <= col_offset_r ;
						/*Assign proper states for next cycle*/
						if(present_r_state == READ_PAGE_MULTI_PLANE)
						begin
							get_next_addr     <= 1'b1 ;//Need to evaluate next address and keep it ready.
							present_r_state   <= next_r_state ;
							stay_with_decision <= 1'b1 ;
							read_state        <= START_READ ;
						end
						else //READ PAGE case
						begin
							if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b0 &&(last_r_req == 1'b1 || multi_plane_r_pend_2 == 1'b1))
							begin
								multi_plane_r_pend_1 <= 1'b0 ;//Done with this read.
								read_state           <= SELECT_C_R ; //SELECT CACHE REGISTER. since one cache in one plane is already read out.
								page_r_pend_1        <= 1'b0 ;
								rg_interrupt         <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free     <= 1'b0 ; //Make the read FIFO busy.
							end
						    else if(multi_plane_r_pend_1 == 1'b0 && multi_plane_after_page == 1'b0 && multi_plane_r_pend_2 == 1'b1 && last_r_req == 1'b0)
							begin
								multi_plane_r_pend_2 <= 1'b0 ;//Transfer all information to _1 temp variables.
								multi_plane_r_pend_1 <= 1'b1 ;
								mplane_cycl3_buff1 <= mplane_cycl3_buff2 ;
								mplane_cycl4_buff1 <= mplane_cycl4_buff2 ;
								mplane_cycl5_buff1 <= mplane_cycl5_buff2 ;
								page_r_pend_2        <= 1'b0 ;
								page_r_pend_1        <= 1'b1 ;
								a_cycl3_buff1        <= a_cycl3_buff2 ;
								a_cycl4_buff1        <= a_cycl4_buff2 ;
								a_cycl5_buff1        <= a_cycl5_buff2 ;
								present_r_state      <= IDLE ;
								read_state           <= START_READ ;
								get_next_addr        <= 1'b1 ;
								rg_interrupt         <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free     <= 1'b0 ; //Make the read FIFO busy.
							end
							else if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b1 && last_r_req == 1'b0)
							begin
								multi_plane_after_page <= 1'b0 ;
								page_r_pend_2          <= 1'b0 ;
								page_r_pend_1          <= 1'b1 ;
								a_cycl3_buff1          <= a_cycl3_buff2 ;
								a_cycl4_buff1          <= a_cycl4_buff2 ;
								a_cycl5_buff1          <= a_cycl5_buff2 ;
								present_r_state        <= IDLE ;
								read_state             <= START_READ ;
								get_next_addr          <= 1'b1 ;
								rg_interrupt           <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free       <= 1'b0 ; //Make the read FIFO busy.
							end
							else if(multi_plane_r_pend_1 == 1'b1 && multi_plane_after_page == 1'b1)
							begin
								read_state             <= ASK_STATUS1 ; //Start polling actual multi-plane op.
								multi_plane_after_page <= 1'b0 ; 
								rg_interrupt           <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free       <= 1'b0 ; //Make the read FIFO busy.
								if(page_r_pend_2 == 1'b1) //1st page is already read when here.
								begin
									page_r_pend_2 <= 1'b0 ;
									a_cycl3_buff1 <= a_cycl3_buff2 ;
									a_cycl4_buff1 <= a_cycl4_buff2 ;
									a_cycl5_buff1 <= a_cycl5_buff2 ;
								end
							end
							else if(last_r_req == 1'b1 && new_r_req == 1'b1) //Corresponds to one page request
							begin
								last_r_req      <= 1'b0 ;
								flag_end_read   <= 1'b1 ;
								new_r_req       <= 1'b0 ;
								page_r_pend_1   <= 1'b0 ;
								present_r_state <= IDLE ;
								read_state      <= START_READ ;
								rg_interrupt    <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free	  <= 1'b0 ; //Make the read FIFO busy.
							end
							else if(new_r_req == 1'b1)
							begin
								new_r_req       <= 1'b0 ;
								get_next_addr   <= 1'b1 ;//Need to evaluate next address and keep it ready.
								present_r_state <= IDLE ;
								read_state      <= START_READ ;
							end
							else // if(last_r_req == 1'b1 && new_r_req == 1'b0)
							begin
								if(page_r_pend_2 == 1'b1) //1st page is already read when here.
								begin
									if(multi_plane_r_pend_2 == 1'b1)
									begin
										multi_plane_r_pend_1 <= 1'b1 ;
										multi_plane_r_pend_2 <= 1'b0 ;
										mplane_cycl3_buff1 <= mplane_cycl3_buff2 ;
										mplane_cycl4_buff1 <= mplane_cycl4_buff2 ;
										mplane_cycl5_buff1 <= mplane_cycl4_buff2 ;
									end
									page_r_pend_2 <= 1'b0 ;
									a_cycl3_buff1 <= a_cycl3_buff2 ;
									a_cycl4_buff1 <= a_cycl4_buff2 ;
									a_cycl5_buff1 <= a_cycl4_buff2 ;
									read_state    <= ASK_STATUS1;
								end
								else
								begin
									last_r_req      <= 1'b0 ;
									flag_end_read   <= 1'b1 ;
									present_r_state <= IDLE ;
								end
								rg_interrupt                     <= 1'b1 ; //Since it is Dreg will reset in next cycle.Hence a pulse.
								data_r_fifo_free                 <= 1'b0 ; //Make the read FIFO busy.
							end
						end
					end
		endcase
	endrule	

	rule rl_send_data_to_nvm (data_r_fifo_free  == 1'b0 && data_r_fifo.notEmpty == True) ;
		rg_data_to_nvm	<= data_r_fifo.first ;
		data_r_fifo.deq ;
		if(q_data_count_g < `FIFO_ROWS)
			q_data_count_g <= q_data_count_g + 'h1 ;
		else 
		begin
			q_data_count_g    <= 'h0 ;
			data_r_fifo_free  <= 1'b1 ; //Make the write FIFO free.
			//This is to help ready/busy decision making
			if(flag_end_read   == 1'b1)
			begin
				flag_end_read   <= 1'b0 ;
				read_pending    <= 1'b0 ;
			end
		end
	endrule
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////                                             Rule for Block erase                                                                                  ///////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rule rl_block_erase_initiate_get_address (wr_nand_ce_n == 1'b0 && wr_nand_erase == 1'b1) ;
		chip_sel        <= wr_address_from_nvm[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH+1];//most MSB bit selects which chip is to e written into
		addr_register   <= fn_map_address(wr_address_from_nvm) ;
		cal_block_addr  <= 1'b1 ;
	endrule
	
	rule rl_calculate_block_address (cal_block_addr == 1'b1) ;
		Bit#(`PAGE_WIDTH) page_addr = 'h0 ;
		Bit#(2) block_map      = addr_register[`COLUMN_WIDTH+`PAGE_WIDTH+1:`COLUMN_WIDTH+`PAGE_WIDTH];
		Bit#(TAdd#(TAdd#(`BLOCK_WIDTH,`PLANE_WIDTH),`LUN_WIDTH)) block_addr_0 = addr_register[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH-1:`COLUMN_WIDTH+`PAGE_WIDTH];
		Bit#(TAdd#(TAdd#(`BLOCK_WIDTH,`PLANE_WIDTH),`LUN_WIDTH)) block_addr_1 = {~addr_register[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH+`LUN_WIDTH-1],addr_register[`COLUMN_WIDTH+`PAGE_WIDTH+`BLOCK_WIDTH+`PLANE_WIDTH-1:`COLUMN_WIDTH+`PAGE_WIDTH]};
/*Each block is being split in 4 planes.Hence we can get an adress for block erase from any of these planes.We need to map this recieved address into 4 address in 4 diff planes for multi-plane block erase. Also we need to use the erase multi plane command, hence the addresses are taken in pairs*/
		case(block_map)
			'h0 : 	begin
					Bit#(32) total_addr0 = {'h0,block_addr_0,page_addr};
					erase_addr_cycl3[0] <= total_addr0[7:0] ;
					erase_addr_cycl4[0] <= total_addr0[15:8] ;
					erase_addr_cycl5[0] <= total_addr0[23:16] ;
					Bit#(32) total_addr1 = {'h0,(block_addr_0+'h3),page_addr};
					erase_addr_cycl3[1] <= total_addr1[7:0] ;
					erase_addr_cycl4[1] <= total_addr1[15:8] ;
					erase_addr_cycl5[1] <= total_addr1[23:16] ;
				
					Bit#(32) total_addr2 = {'h0,block_addr_1,page_addr};
					erase_addr_cycl3[2] <= total_addr2[7:0] ;
					erase_addr_cycl4[2] <= total_addr2[15:8] ;
					erase_addr_cycl5[2] <= total_addr2[23:16] ;
					Bit#(32) total_addr3 = {'h0,block_addr_1+'h3,page_addr};
					erase_addr_cycl3[3] <= total_addr3[7:0] ;
					erase_addr_cycl4[3] <= total_addr3[15:8] ;
					erase_addr_cycl5[3] <= total_addr3[23:16] ;
				end
			'h1 :	begin
					Bit#(32) total_addr0 = {'h0,block_addr_0,page_addr};
					erase_addr_cycl3[1] <= total_addr0[7:0] ;
					erase_addr_cycl4[1] <= total_addr0[15:8] ;
					erase_addr_cycl5[1] <= total_addr0[23:16] ;
					Bit#(32) total_addr1 = {'h0,(block_addr_0+'h1),page_addr};
					erase_addr_cycl3[0] <= total_addr1[7:0] ;
					erase_addr_cycl4[0] <= total_addr1[15:8] ;
					erase_addr_cycl5[0] <= total_addr1[23:16] ;
				
					Bit#(32) total_addr2 = {'h0,block_addr_1,page_addr};
					erase_addr_cycl3[3] <= total_addr2[7:0] ;
					erase_addr_cycl4[3] <= total_addr2[15:8] ;
					erase_addr_cycl5[3] <= total_addr2[23:16] ;
					Bit#(32) total_addr3 = {'h0,block_addr_1+'h1,page_addr};
					erase_addr_cycl3[2] <= total_addr3[7:0] ;
					erase_addr_cycl4[2] <= total_addr3[15:8] ;
					erase_addr_cycl5[2] <= total_addr3[23:16] ;
				end
			'h2 :	begin
					Bit#(32) total_addr0 = {'h0,block_addr_0,page_addr};
					erase_addr_cycl3[0] <= total_addr0[7:0] ;
					erase_addr_cycl4[0] <= total_addr0[15:8] ;
					erase_addr_cycl5[0] <= total_addr0[23:16] ;
					Bit#(32) total_addr1 = {'h0,(block_addr_0-'h1),page_addr};
					erase_addr_cycl3[1] <= total_addr1[7:0] ;
					erase_addr_cycl4[1] <= total_addr1[15:8] ;
					erase_addr_cycl5[1] <= total_addr1[23:16] ;
				
					Bit#(32) total_addr2 = {'h0,block_addr_1,page_addr};
					erase_addr_cycl3[2] <= total_addr2[7:0] ;
					erase_addr_cycl4[2] <= total_addr2[15:8] ;
					erase_addr_cycl5[2] <= total_addr2[23:16] ;
					Bit#(32) total_addr3 = {'h0,block_addr_1-'h1,page_addr};
					erase_addr_cycl3[3] <= total_addr3[7:0] ;
					erase_addr_cycl4[3] <= total_addr3[15:8] ;
					erase_addr_cycl5[3] <= total_addr3[23:16] ;
				end
		    default :	begin
		    			Bit#(32) total_addr0 = {'h0,block_addr_0,page_addr};
					erase_addr_cycl3[1] <= total_addr0[7:0] ;
					erase_addr_cycl4[1] <= total_addr0[15:8] ;
					erase_addr_cycl5[1] <= total_addr0[23:16] ;
					Bit#(32) total_addr1 = {'h0,(block_addr_0-'h3),page_addr};
					erase_addr_cycl3[0] <= total_addr1[7:0] ;
					erase_addr_cycl4[0] <= total_addr1[15:8] ;
					erase_addr_cycl5[0] <= total_addr1[23:16] ;
				
					Bit#(32) total_addr2 = {'h0,block_addr_1,page_addr};
					erase_addr_cycl3[3] <= total_addr2[7:0] ;
					erase_addr_cycl4[3] <= total_addr2[15:8] ;
					erase_addr_cycl5[3] <= total_addr2[23:16] ;
					Bit#(32) total_addr3 = {'h0,block_addr_1-'h3,page_addr};
					erase_addr_cycl3[2] <= total_addr3[7:0] ;
					erase_addr_cycl4[2] <= total_addr3[15:8] ;
					erase_addr_cycl5[2] <= total_addr3[23:16] ;
		    		end
		endcase
		
	
		block_erase_ongoing <= 1'b1 ;
		cal_block_addr      <= 1'b0 ;
	endrule

	rule rl_block_erase_start(block_erase_ongoing == 1'b1) ;
		case(erase_state)
		         ASK_STATUS1 : 	begin
					   	/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h78 ;
						erase_state             <= P_ADDR_S ;
				       	end
			 P_ADDR_S    :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl3[0] ;
						else
							rg_data_to_flash        <= erase_addr_cycl3[2] ;
						erase_state             <= B_ADDR_S ;
					end
			 B_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl4[0] ;
						else
							rg_data_to_flash        <= erase_addr_cycl4[2] ;
						erase_state           <= L_ADDR_S ;
					end
			L_ADDR_S    : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl5[0] ;
						else
							rg_data_to_flash        <= erase_addr_cycl5[2] ;
						erase_state           <= ENABLE_S_READ1 ;
					end
		      ENABLE_S_READ1 :  begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= WAIT1 ;
					end
			 WAIT1       :   begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= READ_STATUS1 ;
					end
		        READ_STATUS1 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(last_status_r == 1'b1) //For checking if erase failed or not.
						begin
							if(wr_data_from_flash[5] == 1'b1) //Check ARDY bit
							begin
								block_erase_ongoing <= 'h0 ;
								erase_state  <= ASK_STATUS1 ;
								if(wr_data_from_flash[0] == 1'b1) //FAILC bit
									rg_erase_fail       <= 1'b1 ;
								else
									rg_erase_success    <= 1'b1 ;
							end
							else
								erase_state  <= ASK_STATUS2 ; 
						end
						else
						begin
							/*Here we did not have a cache operation , hence need to check both RDY and ARDY flags*/
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
								erase_state  <= START_ERASE ;
							else
								erase_state  <= ASK_STATUS2 ; 
						end
						
					end
			ASK_STATUS2  :  begin
					/*If LUN is busy dont waste 6 cycles by polling using 78h instead use 70h for polling*/
						/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h70 ;
						erase_state             <= ENABLE_S_READ2 ;
					end
		      ENABLE_S_READ2 :  begin
		      				rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= WAIT2 ; 
					end
			    WAIT2    :  begin
			    			rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= READ_STATUS2 ;
					end
		        READ_STATUS2 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(last_status_r == 1'b1) //For checking if erase failed or not.
						begin
							if(wr_data_from_flash[5] == 1'b1) //Check ARDY bit
							begin
								block_erase_ongoing <= 'h0 ;
								erase_state  <= ASK_STATUS1 ;
								if(wr_data_from_flash[0] == 1'b1) //FAILC bit
									rg_erase_fail       <= 1'b1 ;
								else
									rg_erase_success    <= 1'b1 ;
							end
							else
								erase_state  <= ASK_STATUS2 ; 
						end
						else
						begin
							/*Here we did not have a cache operation , hence need to check both RDY and ARDY flags*/
							if(wr_data_from_flash[6] == 1'b1 && wr_data_from_flash[5] == 1'b1)
								erase_state  <= START_ERASE ;
							else
								erase_state  <= ASK_STATUS2 ; 
						end
					end
		       START_ERASE   :	begin
		       				/*Send 60h command series from here*/	
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h60 ;
						erase_state             <= P_ADDR ;
					end
			     P_ADDR  :  begin
			     			/*Send page address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl3[0] ;
						else if(erase_cnt == 'h1)
							rg_data_to_flash        <= erase_addr_cycl3[1] ;
						else if(erase_cnt == 'h2)
							rg_data_to_flash        <= erase_addr_cycl3[2] ;
						else
							rg_data_to_flash        <= erase_addr_cycl3[3] ;
						erase_state             <= B_ADDR ; 
					end
			     B_ADDR  :  begin
			     			/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl4[0] ;
						else if(erase_cnt == 'h1)
							rg_data_to_flash        <= erase_addr_cycl4[1] ;
						else if(erase_cnt == 'h2)
							rg_data_to_flash        <= erase_addr_cycl4[2] ;
						else
							rg_data_to_flash        <= erase_addr_cycl4[3] ;
						erase_state             <= L_ADDR ; 
					end
			     L_ADDR  :  begin
			     			/*Send block address for program*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b1 ;
						if(erase_cnt == 'h0)
							rg_data_to_flash        <= erase_addr_cycl5[0] ;
						else if(erase_cnt == 'h1)
							rg_data_to_flash        <= erase_addr_cycl5[1] ;
						else if(erase_cnt == 'h2)
							rg_data_to_flash        <= erase_addr_cycl5[2] ;
						else
							rg_data_to_flash        <= erase_addr_cycl5[3] ;
						erase_state             <= END_COMMAND ; 
					end
		        END_COMMAND  : 	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						if(erase_cnt[0] == 'h0)
						begin
							rg_data_to_flash        <= 'hD1 ;
							erase_state             <= START_ERASE ;
							erase_cnt               <= erase_cnt + 'h1 ;
						end
						else if(erase_cnt == 'h1)
						begin
							rg_data_to_flash        <= 'hD0 ;
							erase_state             <= ASK_STATUS1 ;
							erase_cnt               <= erase_cnt + 'h1 ;
						end
						else
						begin
							rg_data_to_flash        <= 'hD0 ;
							erase_state             <= FINISH_STATUS ;
							erase_cnt               <= 'h0 ;
						end
					end
		      FINISH_STATUS  :  begin
						/*Check status of the LUN*/
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b0 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b1 ;
						rg_onfi_ale             <= 1'b0 ;
						rg_data_to_flash        <= 'h70 ;
						erase_state             <= ENABLE_S_READ3 ;
					end
		      ENABLE_S_READ3 :  begin
		      				rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b0 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= WAIT3 ; 
					end
			    WAIT3    :  begin
			    			rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						erase_state             <= READ_STATUS3 ;
					end
		        READ_STATUS3 :	begin
						rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
						rg_onfi_ce_n[~chip_sel] <= 1'b1 ;
						rg_onfi_we_n            <= 1'b1 ;
						rg_onfi_re_n            <= 1'b1 ;
						rg_onfi_cle             <= 1'b0 ;
						rg_onfi_ale             <= 1'b0 ;
						if(wr_data_from_flash[5] == 1'b1) //Check ARDY bit
						begin
							erase_state         <= ASK_STATUS1 ;
							if(wr_data_from_flash[0] == 1'b1) //FAILC bit
							begin
								rg_erase_fail       <= 1'b1 ;
								block_erase_ongoing <= 'h0 ;
							end
							else
								last_status_r        <= 1'b1 ; 
						end
						else
							erase_state  <= FINISH_STATUS ; 
					end
		endcase
	endrule
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////                                             Rule for reset                                                                                       ///////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rule rl_reset (reset_flag == 1'b1 && wr_ready_busy_n[0] == 1'b1 && wr_ready_busy_n[1] == 1'b1) ; //Reset has to be sent only when the flash memory is not busy.
		if(reset_applied == 1'b1)
		begin
		/*If reset has been applied in prev cycle and the wr_ready_busy_n takes some time to update(3-4 cycles)*/
			if(reset_ongoing == 1'b1)
			begin
				reset_ongoing <= 1'b0 ;
				reset_applied <= 1'b0 ;
				reset_flag    <= 1'b0 ;
			end
			else
			begin
				reset_applied <= 1'b1 ;
				reset_flag    <= 1'b1 ;
			end	
		end
		else
		begin
			rg_onfi_ce_n[chip_sel]  <= 1'b0 ;
			rg_onfi_ce_n[~chip_sel] <= 1'b0 ;
			rg_onfi_we_n            <= 1'b0 ;
			rg_onfi_re_n            <= 1'b1 ;
			rg_onfi_cle             <= 1'b0 ;
			rg_onfi_ale             <= 1'b1 ;
			rg_data_to_flash        <= 'hFF ;
			reset_applied           <= 1'b1 ;
		end
	endrule
	
	rule rl_reset_state_track (reset_flag == 1'b1 && wr_ready_busy_n[0] == 'h0 && wr_ready_busy_n[1] == 'h0) ; //If reset has started and chip is busy.
		if(reset_applied == 1'b1)
			reset_ongoing <= 1'b1 ;
		else
			reset_ongoing <= 1'b0 ;
	endrule
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////                                                                 Busy flag updation                                                              /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rule rl_ready_busy_update ;
		//For write , if both FIFOs are busy, busy will be enabled,  indicating no data can be taken during that time.
		//rg_ready_busy_n <= (data_w_fifo_free[0]|data_w_fifo_free[1])&(~(read_pending|block_erase_ongoing|reset_flag)) ;
		rg_ready_busy_n <= (data_w_fifo_free)&(~(read_pending|block_erase_ongoing)) ;
	endrule

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface nvm_nfc_interface  = fn_nvm_nfc_interface ( wr_address_from_nvm, wr_data_from_nvm, wr_w_length, wr_r_length,rg_data_to_nvm, wr_nand_ce_n, wr_nand_we_n, wr_nand_re_n, rg_interrupt, rg_ready_busy_n, wr_nand_erase, rg_write_success, rg_write_fail, rg_erase_success, rg_erase_fail) ;

interface nfc_onfi_interface = fn_nfc_onfi_interface ( rg_onfi_ce_n[0], rg_onfi_ce_n[1], rg_onfi_cle, rg_onfi_ale, rg_onfi_we_n, rg_onfi_re_n, rg_onfi_wp_n, rg_data_to_flash, wr_data_from_flash, wr_ready_busy_n[0], wr_ready_busy_n[1]) ;


endmodule: mkNandFlashController

endpackage 
