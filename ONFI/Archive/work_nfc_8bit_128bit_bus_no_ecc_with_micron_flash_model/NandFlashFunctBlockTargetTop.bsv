package NandFlashFunctBlockTargetTop ;
//Idea of this package is to hold 2 target chips as a single block

interface ONFI_Target_Interface_top ;
	method Action _onfi_ce_n_0 ( bit _onfi_ce_n ) ;
	method Action _onfi_we_n_0 ( bit _onfi_we_n ) ;
	method Action _onfi_re_n_0 ( bit _onfi_re_n ) ;
	method Action _onfi_wp_n_0 ( bit _onfi_wp_n ) ;
	method Action _onfi_cle_0 ( bit _onfi_cle ) ;
	method Action _onfi_ale_0 ( bit _onfi_ale ) ;
	method bit t_ready_busy_n_0 ;
	method Action _onfi_ce_n_1 ( bit _onfi_ce_n ) ;
	method Action _onfi_we_n_1 ( bit _onfi_we_n ) ;
	method Action _onfi_re_n_1 ( bit _onfi_re_n ) ;
	method Action _onfi_wp_n_1 ( bit _onfi_wp_n ) ;
	method Action _onfi_cle_1 ( bit _onfi_cle ) ;
	method Action _onfi_ale_1 ( bit _onfi_ale ) ;
	method bit t_ready_busy_n_1 ;
	
	method Action _onfi_ENi ( bit _onfi_ENi ) ;
	method bit _onfi_ENo ;
	method Action _onfi_Re_c ( bit _onfi_Re_c ) ;
	
	interface Inout#(Bit#(8)) data0 ;
	interface Inout#(Bit#(8)) data1 ;
	
        interface Inout#(Bool) dqs_open;
        interface Inout#(Bool) dqs_c_open;
    
endinterface

	import "BVI" nand_model =
module mkNandFlashTargetTop(ONFI_Target_Interface_top ifc) ;

	//default_clock no_clk; 
   	default_reset no_reset;
	
	ifc_inout data0(Dq_Io);
	ifc_inout data1(Dq_Io2);
	
	ifc_inout dqs_open(Dqs);
	ifc_inout dqs_c_open(Dqs_c);
	
	method _onfi_ce_n_0(Ce_n) enable((*inhigh*) en1);
	method _onfi_we_n_0(Clk_We_n) enable((*inhigh*) en2);
	method _onfi_re_n_0(Wr_Re_n) enable((*inhigh*) en3);
	method _onfi_wp_n_0(Wp_n) enable((*inhigh*) en4);
	method _onfi_cle_0(Cle) enable((*inhigh*) en5);
	method _onfi_ale_0(Ale) enable((*inhigh*) en6);
	method Rb_n t_ready_busy_n_0 ();
	
	method _onfi_ce_n_1(Ce2_n) enable((*inhigh*) en7);
	method _onfi_we_n_1(Clk_We2_n) enable((*inhigh*) en8);
	method _onfi_re_n_1(Wr_Re2_n) enable((*inhigh*) en9);
	method _onfi_wp_n_1(Wp2_n) enable((*inhigh*) en10);
	method _onfi_cle_1(Cle2) enable((*inhigh*) en11);
	method _onfi_ale_1(Ale2) enable((*inhigh*) en12);
	method Rb2_n t_ready_busy_n_1 ();
	
	method _onfi_ENi (ENi) enable((*inhigh*) en13);
	method ENo _onfi_ENo ();
	method _onfi_Re_c (Re_c) enable((*inhigh*) en14);
	
	schedule (_onfi_ce_n_0) CF (_onfi_ce_n_0);
	schedule (_onfi_ce_n_0) CF (_onfi_we_n_0);
	schedule (_onfi_ce_n_0) CF (_onfi_re_n_0);
	schedule (_onfi_ce_n_0) CF (_onfi_wp_n_0);
	schedule (_onfi_ce_n_0) CF (_onfi_cle_0);
	schedule (_onfi_ce_n_0) CF (_onfi_ale_0);
	schedule (_onfi_ce_n_0) CF (t_ready_busy_n_0);
	schedule (_onfi_ce_n_0) CF (_onfi_ce_n_1);
	schedule (_onfi_ce_n_0) CF (_onfi_we_n_1);
	schedule (_onfi_ce_n_0) CF (_onfi_re_n_1);
	schedule (_onfi_ce_n_0) CF (_onfi_wp_n_1);
	schedule (_onfi_ce_n_0) CF (_onfi_cle_1);
	schedule (_onfi_ce_n_0) CF (_onfi_ale_1);
	schedule (_onfi_ce_n_0) CF (t_ready_busy_n_1);
	schedule (_onfi_ce_n_0) CF (_onfi_ENi);
	schedule (_onfi_ce_n_0) CF (_onfi_ENo);
	schedule (_onfi_ce_n_0) CF (_onfi_Re_c);
	
	schedule (_onfi_we_n_0) CF (_onfi_we_n_0);
	schedule (_onfi_we_n_0) CF (_onfi_re_n_0);
	schedule (_onfi_we_n_0) CF (_onfi_wp_n_0);
	schedule (_onfi_we_n_0) CF (_onfi_cle_0);
	schedule (_onfi_we_n_0) CF (_onfi_ale_0);
	schedule (_onfi_we_n_0) CF (t_ready_busy_n_0);
	schedule (_onfi_we_n_0) CF (_onfi_ce_n_1);
	schedule (_onfi_we_n_0) CF (_onfi_we_n_1);
	schedule (_onfi_we_n_0) CF (_onfi_re_n_1);
	schedule (_onfi_we_n_0) CF (_onfi_wp_n_1);
	schedule (_onfi_we_n_0) CF (_onfi_cle_1);
	schedule (_onfi_we_n_0) CF (_onfi_ale_1);
	schedule (_onfi_we_n_0) CF (t_ready_busy_n_1);
	schedule (_onfi_we_n_0) CF (_onfi_ENi);
	schedule (_onfi_we_n_0) CF (_onfi_ENo);
	schedule (_onfi_we_n_0) CF (_onfi_Re_c);
	
	schedule (_onfi_re_n_0) CF (_onfi_re_n_0);
	schedule (_onfi_re_n_0) CF (_onfi_wp_n_0);
	schedule (_onfi_re_n_0) CF (_onfi_cle_0);
	schedule (_onfi_re_n_0) CF (_onfi_ale_0);
	schedule (_onfi_re_n_0) CF (t_ready_busy_n_0);
	schedule (_onfi_re_n_0) CF (_onfi_ce_n_1);
	schedule (_onfi_re_n_0) CF (_onfi_we_n_1);
	schedule (_onfi_re_n_0) CF (_onfi_re_n_1);
	schedule (_onfi_re_n_0) CF (_onfi_wp_n_1);
	schedule (_onfi_re_n_0) CF (_onfi_cle_1);
	schedule (_onfi_re_n_0) CF (_onfi_ale_1);
	schedule (_onfi_re_n_0) CF (t_ready_busy_n_1);
	schedule (_onfi_re_n_0) CF (_onfi_ENi);
	schedule (_onfi_re_n_0) CF (_onfi_ENo);
	schedule (_onfi_re_n_0) CF (_onfi_Re_c);
	
	schedule (_onfi_wp_n_0) CF (_onfi_wp_n_0);
	schedule (_onfi_wp_n_0) CF (_onfi_cle_0);
	schedule (_onfi_wp_n_0) CF (_onfi_ale_0);
	schedule (_onfi_wp_n_0) CF (t_ready_busy_n_0);
	schedule (_onfi_wp_n_0) CF (_onfi_ce_n_1);
	schedule (_onfi_wp_n_0) CF (_onfi_we_n_1);
	schedule (_onfi_wp_n_0) CF (_onfi_re_n_1);
	schedule (_onfi_wp_n_0) CF (_onfi_wp_n_1);
	schedule (_onfi_wp_n_0) CF (_onfi_cle_1);
	schedule (_onfi_wp_n_0) CF (_onfi_ale_1);
	schedule (_onfi_wp_n_0) CF (t_ready_busy_n_1);
	schedule (_onfi_wp_n_0) CF (_onfi_ENi);
	schedule (_onfi_wp_n_0) CF (_onfi_ENo);
	schedule (_onfi_wp_n_0) CF (_onfi_Re_c);
	
	schedule (_onfi_cle_0) CF (_onfi_cle_0);
	schedule (_onfi_cle_0) CF (_onfi_ale_0);
	schedule (_onfi_cle_0) CF (t_ready_busy_n_0);
	schedule (_onfi_cle_0) CF (_onfi_ce_n_1);
	schedule (_onfi_cle_0) CF (_onfi_we_n_1);
	schedule (_onfi_cle_0) CF (_onfi_re_n_1);
	schedule (_onfi_cle_0) CF (_onfi_wp_n_1);
	schedule (_onfi_cle_0) CF (_onfi_cle_1);
	schedule (_onfi_cle_0) CF (_onfi_ale_1);
	schedule (_onfi_cle_0) CF (t_ready_busy_n_1);
	schedule (_onfi_cle_0) CF (_onfi_ENi);
	schedule (_onfi_cle_0) CF (_onfi_ENo);
	schedule (_onfi_cle_0) CF (_onfi_Re_c);
	
	schedule (_onfi_ale_0) CF (_onfi_ale_0);
	schedule (_onfi_ale_0) CF (t_ready_busy_n_0);
	schedule (_onfi_ale_0) CF (_onfi_ce_n_1);
	schedule (_onfi_ale_0) CF (_onfi_we_n_1);
	schedule (_onfi_ale_0) CF (_onfi_re_n_1);
	schedule (_onfi_ale_0) CF (_onfi_wp_n_1);
	schedule (_onfi_ale_0) CF (_onfi_cle_1);
	schedule (_onfi_ale_0) CF (_onfi_ale_1);
	schedule (_onfi_ale_0) CF (t_ready_busy_n_1);
	schedule (_onfi_ale_0) CF (_onfi_ENi);
	schedule (_onfi_ale_0) CF (_onfi_ENo);
	schedule (_onfi_ale_0) CF (_onfi_Re_c);
	
	schedule (t_ready_busy_n_0) CF (t_ready_busy_n_0);
	schedule (t_ready_busy_n_0) CF (_onfi_ce_n_1);
	schedule (t_ready_busy_n_0) CF (_onfi_we_n_1);
	schedule (t_ready_busy_n_0) CF (_onfi_re_n_1);
	schedule (t_ready_busy_n_0) CF (_onfi_wp_n_1);
	schedule (t_ready_busy_n_0) CF (_onfi_cle_1);
	schedule (t_ready_busy_n_0) CF (_onfi_ale_1);
	schedule (t_ready_busy_n_0) CF (t_ready_busy_n_1);
	schedule (t_ready_busy_n_0) CF (_onfi_ENi);
	schedule (t_ready_busy_n_0) CF (_onfi_ENo);
	schedule (t_ready_busy_n_0) CF (_onfi_Re_c);
	
	schedule (_onfi_ce_n_1) CF (_onfi_ce_n_1);
	schedule (_onfi_ce_n_1) CF (_onfi_we_n_1);
	schedule (_onfi_ce_n_1) CF (_onfi_re_n_1);
	schedule (_onfi_ce_n_1) CF (_onfi_wp_n_1);
	schedule (_onfi_ce_n_1) CF (_onfi_cle_1);
	schedule (_onfi_ce_n_1) CF (_onfi_ale_1);
	schedule (_onfi_ce_n_1) CF (t_ready_busy_n_1);
	schedule (_onfi_ce_n_1) CF (_onfi_ENi);
	schedule (_onfi_ce_n_1) CF (_onfi_ENo);
	schedule (_onfi_ce_n_1) CF (_onfi_Re_c);
	
	schedule (_onfi_we_n_1) CF (_onfi_we_n_1);
	schedule (_onfi_we_n_1) CF (_onfi_re_n_1);
	schedule (_onfi_we_n_1) CF (_onfi_wp_n_1);
	schedule (_onfi_we_n_1) CF (_onfi_cle_1);
	schedule (_onfi_we_n_1) CF (_onfi_ale_1);
	schedule (_onfi_we_n_1) CF (t_ready_busy_n_1);
	schedule (_onfi_we_n_1) CF (_onfi_ENi);
	schedule (_onfi_we_n_1) CF (_onfi_ENo);
	schedule (_onfi_we_n_1) CF (_onfi_Re_c);
	
	schedule (_onfi_re_n_1) CF (_onfi_re_n_1);
	schedule (_onfi_re_n_1) CF (_onfi_wp_n_1);
	schedule (_onfi_re_n_1) CF (_onfi_cle_1);
	schedule (_onfi_re_n_1) CF (_onfi_ale_1);
	schedule (_onfi_re_n_1) CF (t_ready_busy_n_1);
	schedule (_onfi_re_n_1) CF (_onfi_ENi);
	schedule (_onfi_re_n_1) CF (_onfi_ENo);
	schedule (_onfi_re_n_1) CF (_onfi_Re_c);
	
	schedule (_onfi_wp_n_1) CF (_onfi_wp_n_1);
	schedule (_onfi_wp_n_1) CF (_onfi_cle_1);
	schedule (_onfi_wp_n_1) CF (_onfi_ale_1);
	schedule (_onfi_wp_n_1) CF (t_ready_busy_n_1);
	schedule (_onfi_wp_n_1) CF (_onfi_ENi);
	schedule (_onfi_wp_n_1) CF (_onfi_ENo);
	schedule (_onfi_wp_n_1) CF (_onfi_Re_c);
	
	schedule (_onfi_cle_1) CF (_onfi_cle_1);
	schedule (_onfi_cle_1) CF (_onfi_ale_1);
	schedule (_onfi_cle_1) CF (t_ready_busy_n_1);
	schedule (_onfi_cle_1) CF (_onfi_ENi);
	schedule (_onfi_cle_1) CF (_onfi_ENo);
	schedule (_onfi_cle_1) CF (_onfi_Re_c);
	
	schedule (_onfi_ale_1) CF (_onfi_ale_1);
	schedule (_onfi_ale_1) CF (t_ready_busy_n_1);
	schedule (_onfi_ale_1) CF (_onfi_ENi);
	schedule (_onfi_ale_1) CF (_onfi_ENo);
	schedule (_onfi_ale_1) CF (_onfi_Re_c);
	
	schedule (t_ready_busy_n_1) CF (t_ready_busy_n_1);
	schedule (_onfi_ale_1) CF (_onfi_ENi);
	schedule (_onfi_ale_1) CF (_onfi_ENo);
	schedule (_onfi_ale_1) CF (_onfi_Re_c);
	
	schedule (_onfi_ENi) CF (_onfi_ENi);
	schedule (_onfi_ENi) CF (t_ready_busy_n_1);
	schedule (_onfi_ENi) CF (_onfi_ENo);
	schedule (_onfi_ENi) CF (_onfi_Re_c);
	
	schedule (_onfi_ENo) CF (_onfi_ENo);
	schedule (_onfi_ENo) CF (_onfi_Re_c);
	schedule (_onfi_ENo) CF (t_ready_busy_n_1);
	
	schedule (_onfi_Re_c) CF (_onfi_Re_c);
	schedule (_onfi_Re_c) CF (t_ready_busy_n_1);
	

endmodule
endpackage
